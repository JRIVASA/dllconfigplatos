Attribute VB_Name = "modConfigPlatos"

Public mAplicaTRPend As Boolean
Private mConfig As clsConfigPlatos

Public Enum Enum_FieldType
    Numerico
    Alfanumerico
End Enum

Global WindowsArchitecture As OperatingSystemArchitecture

Public Uno As Boolean
Public sFactor As Variant
Public sDecMoneda As Integer

Public Retorno As Boolean

Public FrmAppLink As Object

Sub Main()
    
    'If App.PrevInstance Then End ' NO COMPILA
    
    Set mConfig = New clsConfigPlatos
    
    mConfig.IniciarInterfaz "srv-datos\cliente2012", etiConfiguracion
    
    Set mConfig = Nothing
    
End Sub

Public Sub MostrarEditorTexto(pFrm As Form, pGrd As Object, ByRef txteditor As Object, _
ByRef cellRow As Long, ByRef cellCol As Long, pKeyAscii As Integer, _
Optional pNumerico As Boolean = False, Optional pAutoTecla As Boolean = True)
    
    Dim mEscribio As Boolean
    
    If txteditor.Visible Then Exit Sub
    txteditor.Alignment = IIf(pNumerico, 1, 0)
    
    With pGrd

        .RowSel = .Row
        .ColSel = .Col
        
        a = .Left + .CellLeft
        b = .Top + .CellTop
        
        txteditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - pFrm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - pFrm.ScaleY(1, vbPixels, vbTwips)
                        
        txteditor.Font = .Font
        txteditor.Text = .Text
        
        If pAutoTecla Then
            If pNumerico Then
                If IsNumeric(Chr(pKeyAscii)) Then
                    txteditor.Text = Chr(pKeyAscii)
                    mEscribio = True
                End If
            Else
                If pKeyAscii > 32 Then
                    txteditor.Text = Chr(pKeyAscii)
                    mEscribio = True
                End If
            End If
        End If
        
        txteditor.Visible = True
        txteditor.ZOrder
        
        If PuedeObtenerFoco(txteditor) Then txteditor.SetFocus
        
        If mEscribio Then
            txteditor.SelStart = Len(txteditor)
        Else
            txteditor.SelStart = 0
            txteditor.SelLength = Len(txteditor)
        End If
            
        cellRow = .Row
        cellCol = .Col

     End With

End Sub

'Public Function InterfazBusquedaProducto(Conexion As ADODB.Connection, Titulo As String, Optional SoloCompuesto As Boolean)
'    Dim mBusqueda As Object
'    Dim mSql As String
'
'    On Error GoTo Errores
'    Set mBusqueda = CreateObject("recsun.obj_busqueda")
'    mSql = "Select distinct p.c_codigo,p.c_descri from ma_productos p inner join ma_codigos c on c.c_codnasa=p.c_codigo " _
'        & IIf(SoloCompuesto, " where n_tipopeso=5", " where n_tipopeso<3")
'    With mBusqueda
'        .Inicializar mSql, Titulo, Conexion.ConnectionString
'        .Add_Campos Stellar_Mensaje(122), "c_codigo", 2000, 0
'        .Add_Campos Stellar_Mensaje(123), "C_DESCRI", 5000, 0
'        .Add_CamposBusqueda Stellar_Mensaje(123), "C_DESCRI"
'        .Add_CamposBusqueda Stellar_Mensaje(122), "C.C_CODIGO"
'        InterfazBusquedaProducto = .EJECUTAR
'
'    End With
'    Set mBusqueda = Nothing
'    Exit Function
'Errores:
'    'MsgBox Err.Description, vbCritical, "IBusqueda Producto "
'    Mensajes Err.Description
'    Err.Clear
'End Function

Public Function Mensajes(CadMen As String, Optional ByRef BotonOk As Boolean = False) As Boolean
    
    Dim LMensajes As Object
    
'    Set LMensajes = CreateObject("recsun.OBJ_MENSAJERIA")
'    LMensajes.PressBoton = BotonOk
'    Call LMensajes.mensaje(CadMen, BotonOk)
'    BotonOk = LMensajes.PressBoton
'    Mensajes = LMensajes.PressBoton
    
    If Not PrimeraEjecucionDeModal Then
        PrimeraEjecucionDeModal = False
        ModalDisponible = True
    End If
    
    If BotonOk Then
        frm_Mensajeria.Cancelar.Visible = True
    Else
        frm_Mensajeria.Aceptar.Left = frm_Mensajeria.Cancelar.Left
        frm_Mensajeria.Cancelar.Visible = False
    End If
    
    Retorno = False
    
    frm_Mensajeria.Mensaje = CadMen
    frm_Mensajeria.Cancelar.Enabled = True
    
    If ModalDisponible Then
        frm_Mensajeria.Show vbModal
    End If
    
    Mensajes = Retorno
    Set frm_Mensajeria = Nothing
    
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Sub TecladoWindows(Optional pControl As Object)
    
    On Error Resume Next
    
    ' Abrir el Teclado.
    
    Dim ruta As String
    
    If WindowsArchitecture = [32Bits] Then
                    
        ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(ruta) Then res = ShellEx(0, vbNullString, "" & ruta & "", vbNullString, vbNullString, 1)
            
    ElseIf WindowsArchitecture = [64Bits] Then
    
        ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramW6432") & "\Common Files\Microsoft Shared\Ink")
        
        'MsgBox ruta
        
        If PathExists(ruta) Then
            'MsgBox "existe"
            res = ShellEx(0, vbNullString, "" & ruta & "", vbNullString, vbNullString, 1)
            'MsgBox "Llamada Hecha"
        Else
            
            ruta = FindPath("TabTip32.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles(X86)") & "\Common Files\Microsoft Shared\Ink")
            
            If PathExists(ruta) Then
                'MsgBox "existe ruta 2"
                res = ShellEx(0, vbNullString, "" & ruta & "", vbNullString, vbNullString, 1)
                'MsgBox "llamada hecha"
            End If
            
        End If
        
    End If
    
    If PuedeObtenerFoco(pControl) Then pControl.SetFocus
    
    Exit Sub
    
End Sub

Public Function FindPath(FileName As String, FindFileInUpperLevels As FindFileConstants, Optional BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\") & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then FindPath = CurrentFilePath: Exit Function
        
        FindPath = BaseFilePath + "\" + IIf(FileName Like "*.*", FileName, FileName & "\")
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = BaseFilePath + "\" + IIf(FileName Like "*.*", FileName, FileName & "\")
        
    End If

End Function

Public Function PathExists(pPath As String) As Boolean
    On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then
        If Err.Number = 0 Then
            PathExists = True
        End If
    End If
End Function

Public Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, "\")
    
    If POS <> 0 Then
        GetDirParent = Left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function ShellEx(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    ShellEx = ShellExecute(hWnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd)
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Set Rec = Nothing
End Sub

Public Sub Apertura_RecordsetC(ByRef Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseClient
End Sub

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
    End If
End Sub

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIOA A LA DER
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function DebugColumnViewInfo(pObjTabular As Object) As String
    
    Dim mStr As String
    
    With pObjTabular
        
        If TypeOf pObjTabular Is ListView Then
            
            For Each ColInfo In pObjTabular.ColumnHeaders
                mStr = mStr & GetLines & Rellenar_SpaceR(ColInfo.Width, 15, " ") & Rellenar_SpaceR(ColInfo.Text, 50, " ") & Rellenar_SpaceR(ColInfo.Alignment, 5, " ")
            Next
            
            DebugColumnViewInfo = IIf(Len(mStr) <> 0, mStr, GetLines)
            
            'Debug.Print DebugColumnViewInfo
        
        ElseIf TypeOf pObjTabular Is MSFlexGrid Then
            
            If .Rows > 0 Then
                
                mTmpRow = .Row
                mTmpCol = .Col
                
                .Row = 0
                
                For i = 0 To .Cols - 1
                    .Col = i
                    mStr = mStr & GetLines & Rellenar_SpaceR(.ColWidth(.Col), 15, " ") & Rellenar_SpaceR(.Text, 50, " ") & Rellenar_SpaceR(.CellAlignment, 15, " ") & Rellenar_SpaceR(.ColAlignment(.Col), 15, " ")
                Next
                
                .Row = mTmpRow
                .Col = mTmpCol
                
            End If
            
            DebugColumnViewInfo = IIf(Len(mStr) <> 0, mStr, GetLines)
            
            'Debug.Print DebugColumnViewInfo
        
        End If
        
    End With
    
End Function

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            isDBNull = pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function BuscarInfoProducto_Basica(Optional pConex, _
Optional pPrecioCliente, Optional pGlobal As Boolean = False, _
Optional pDeposito, Optional pTextoABuscar, _
Optional pVista As FrmBuscarProducto_Vista = VistaPrecioCliente, Optional ProductosNoCompuestos As Boolean = True) As Variant
    
    If Not IsMissing(pConex) Then Set FrmBuscarProducto.Conex_SC = pConex
    If Not IsMissing(pPrecioCliente) Then FrmBuscarProducto.PrecioCliente = pPrecioCliente
    
    FrmBuscarProducto.ExistenciaGlobal = pGlobal
    
    If Not IsMissing(pDeposito) Then
        FrmBuscarProducto.DepositoExistencia = CStr(pDeposito)
    'Else
        'FrmBuscarProducto.DepositoExistencia = DPS_Local
    End If
    
    If Not IsMissing(pTextoABuscar) Then
        FrmBuscarProducto.BusquedaInstantanea = True
        FrmBuscarProducto.txtDato.Text = pTextoABuscar
    End If
    
    FrmBuscarProducto.ProductosNoCompuestos = ProductosNoCompuestos
    
    FrmBuscarProducto.Vista = pVista
    
    FrmBuscarProducto.Show vbModal
    
    BuscarInfoProducto_Basica = FrmBuscarProducto.ArrResultado
    
    Set FrmBuscarProducto = Nothing
    
End Function

Public Sub SeleccionarTexto(pControl As Object)
    On Error Resume Next
    pControl.SelStart = 0
    pControl.SelLength = Len(pControl.Text)
End Sub

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Sub LlamarTecladoNumerico(ByRef ValorDef As Double, Decimales As Integer, Optional Titulo As String = "")
    
    Dim Teclado As Object
    
    Set Teclado = CreateObject("DLLKeyboard.DLLTeclado")
    
    Call Teclado.ShowNumPad(ValorDef, Decimales) 'Call Teclado.ShowNumPad(ValorDef, 2, Titulo)
    
    Set Teclado = Nothing
    
End Sub

Public Function InputBoxNumerico(Texto As String, _
Optional Contrasena As Boolean = False, _
Optional pObtenerValorNumerico As Boolean = True, _
Optional pMonto As Double, _
Optional pSoloEntero As Boolean = False) As Variant
    
    On Error GoTo ErroInputBoxN
    
    If pSoloEntero Then
        FrmInputBoxNumerico.Numero = pMonto
        FrmInputBoxNumerico.SoloEntero = pSoloEntero
    End If
    
    If Not FrmInputBoxNumerico.Visible Then
    
        If Contrasena Then
            FrmInputBoxNumerico.TxtPIN.PasswordChar = "*"
        End If
        
        FrmInputBoxNumerico.RequestScroll = Texto
        FrmInputBoxNumerico.Show vbModal
        
        If pObtenerValorNumerico Then
            InputBoxNumerico = FrmInputBoxNumerico.Numero
        Else
            InputBoxNumerico = FrmInputBoxNumerico.NumeroCaption
        End If
        
        Set FrmInputBoxNumerico = Nothing
        
    End If
    
    Exit Function
    
ErroInputBoxN:

    Resume Next
    
End Function

Public Sub SendKeys(ByVal Keys As String, Optional ByVal Wait)
    
    On Error GoTo SendKeysError
    
    If Not IsMissing(Wait) Then
        VBA.Interaction.SendKeys Keys, Wait
    Else
        VBA.Interaction.SendKeys Keys
    End If
    
    Exit Sub
    
SendKeysError:
    
    'Debug.Print err.Description ' Acceso denegado puede ocurrir...
    
    Resume IntentarOtraCosa
    
IntentarOtraCosa:
        
    On Error GoTo OtroError
    
    Dim WshShell As Object
    
    Set WshShell = CreateObject("wscript.shell")
    
    WshShell.SendKeys Keys, Wait
    
    Set WshShell = Nothing
    
    Exit Sub
    
OtroError:
    
    ' ???. Debug.Print err.Description. Prevenir error de runtime...
    
End Sub

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "") As Boolean
    ' pCn > Variant / Object / ADODB.Connection
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTablaV3 = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

Public Function RoundDownFive(ByVal pNumber As Variant, _
Optional ByVal pMaxDec As Integer = 8) As Variant
    
    Dim X0, X1, X2, X3, X4, X5, X6, X7, X8, X9, _
    X10, X11, X12, X13, X14, X15, X16
    
    X0 = Round(pNumber - Fix(pNumber), 8)
    
    If X0 <> 0 Then
        
        X1 = Right(CDec(pNumber), 1)
        
        If X1 = "5" Then
            
            X2 = CDec(Round((CDec(pNumber) - Fix(CDec(pNumber))), 8))
            
            X3 = 0
            
            For X4 = 1 To pMaxDec
                
                X5 = (5 / (10 ^ X4))
                
                X6 = X2 / X5
                
                X7 = Fix(X6)
                
                X8 = Round(X6 - X7, 8)
                
                If X8 = 0 Then
                    
                    X3 = X6
                    Exit For
                    
                End If
                
            Next X4
            
            X9 = (1 / X5 / 2)
            
            If (X2 > 0 And X2 < 1) And X3 > 0 And X9 >= 1 Then
                
                For X10 = 0 To pMaxDec
                    
                    If X10 = pMaxDec Or X9 = (10 ^ X10) Then
                        Exit For
                    End If
                    
                Next
                
                If X10 < pMaxDec Then
                    
                    X11 = 1 / (10 ^ (X10 + 1)) * (5 - 1)
                    
                    X12 = 0
                    
                    For X13 = 0 To (pMaxDec - X10 - 1 - 1)
                        X12 = X12 + (9 * (10 ^ X13))
                    Next X13
                    
                    X14 = (X12 / (10 ^ pMaxDec))
                    X15 = Round(X11 + X14, pMaxDec)
                    
                    X16 = ((X6 - 1) * X5)
                    
                    pNumber = Round(CDec(Fix(pNumber) + X16 + X15), pMaxDec)
                    
                End If
                
            End If
            
        End If
        
    End If
    
    RoundDownFive = pNumber
    
End Function

' pMonto es Variant en vez de Double ya que Double tiene una capacidad Limitada _
de decimales cuando hay muchos enteros. Es decir, o tiene muchos enteros o muchos decimales _
pero no ambos. En cambio un String / Variant puede convertirse al subtipo Decimal por medio _
de la funci�n CDec(Expression) sin perder la precisi�n del n�mero total.

Public Function FormatoDecimalesDinamicos(ByVal pMonto As Variant, _
Optional ByVal pMinimoDecimales As Integer = 0, _
Optional ByVal pMaxDecimales As Integer = 20) As String
    
    Const MaxDec = 25
    
    'pMaxDecimales = 20 por defecto para que sea preciso pero mejor no inventar con _
    mas decimales que esto, sino algo podr�a explotar.
    
    pMaxDecimales = IIf(pMaxDecimales < MaxDec, pMaxDecimales, MaxDec)
    
    If pMinimoDecimales < 0 Then pMinimoDecimales = 0 ' No permitir loqueras que exploten al m�todo.
    If pMaxDecimales < 0 Then pMaxDecimales = pMinimoDecimales
    
    Dim mFormato As String ', FormatoDec As String
    
    'Round(pMonto - Fix(pMonto), MaxDec) ' Explota. Round aguanta m�ximo hasta 22 decimales.
    
    If pMinimoDecimales = 0 And Round(pMonto - Fix(pMonto), 20) = 0 Then
        FormatoDecimalesDinamicos = Format(pMonto, "###,###,##0")
    Else
        If pMaxDecimales = 0 Then
            mFormato = "###,###,##0"
        Else
            mFormato = "###,###,##0." & _
            IIf(pMinimoDecimales > 0, String(pMinimoDecimales, "0"), Empty) & _
            String(pMaxDecimales - pMinimoDecimales, "#")
        End If
        FormatoDecimalesDinamicos = Format(pMonto, mFormato)
    End If
    
End Function

Public Function CampoLength(Campo As String, Tabla As String, _
Server_BD As ADODB.Connection, FieldType As Enum_FieldType, _
Optional MinLength As Integer = 1) As Integer
      
    On Error GoTo FUAAA
    
    Dim RsLength As New ADODB.Recordset

    RsLength.Open "SELECT " & Campo & " FROM " & Tabla & " WHERE 1 = 2", _
    Server_BD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Select Case FieldType
    
    Case Enum_FieldType.Alfanumerico
        CampoLength = RsLength.Fields(Campo).DefinedSize
    Case Enum_FieldType.Numerico
        CampoLength = RsLength.Fields(Campo).Precision
    Case Else
        CampoLength = RsLength.Fields(Campo).DefinedSize
    End Select
    
    If CampoLength < MinLength Then CampoLength = MinLength
    
    RsLength.Close
    
    Exit Function

FUAAA:

    Debug.Print Err.Description
    CampoLength = MinLength
    
End Function

Public Function ManejaSucursales(pConnection) As Boolean
    
    Dim mRstmp As New ADODB.Recordset
    
    SQL = "SELECT COUNT(*) AS Expr1 " & _
    "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_SUCURSALES WHERE (c_Estado = 1)"
    
    Apertura_Recordset mRstmp
    
    mRstmp.Open SQL, pConnection, adOpenForwardOnly, adLockReadOnly
    
    ManejaSucursales = IIf(isDBNull(mRstmp!Expr1, 0) < 1, False, True)
    
    mRstmp.Close
    
    Set mRstmp = Nothing
    
End Function

Public Function ManejaPOS(pConnection) As Boolean
    
    Dim mRstmp As New ADODB.Recordset
    
    SQL = "SELECT Maneja_POS " & _
    "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.REGLAS_COMERCIALES"
    
    Apertura_Recordset mRstmp
    
    mRstmp.Open SQL, pConnection, adOpenForwardOnly, adLockReadOnly
    
    ManejaPOS = IIf(mRstmp!Maneja_POS, True, False)
    
    mRstmp.Close
    
    Set mRstmp = Nothing
    
End Function
