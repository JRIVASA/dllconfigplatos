VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmBuscarProducto 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9240
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   13890
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9240
   ScaleWidth      =   13890
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   7560
      TabIndex        =   16
      Top             =   0
      Visible         =   0   'False
      Width           =   1575
      Begin VB.Image CmdSelect 
         Height          =   300
         Left            =   480
         Picture         =   "FrmBuscarProducto.frx":0000
         Top             =   120
         Width           =   900
      End
   End
   Begin VB.Frame FrameInfo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   5400
      TabIndex        =   15
      Top             =   0
      Visible         =   0   'False
      Width           =   1215
      Begin VB.Image CmdInfo 
         Height          =   300
         Left            =   120
         Picture         =   "FrmBuscarProducto.frx":0384
         Top             =   120
         Width           =   900
      End
   End
   Begin VB.PictureBox Medir 
      Height          =   375
      Left            =   12600
      ScaleHeight     =   315
      ScaleWidth      =   315
      TabIndex        =   12
      Top             =   3720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Timer Tim_Progreso 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   12600
      Top             =   4320
   End
   Begin VB.Frame FrameTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   14040
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   10995
         TabIndex        =   11
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   75
         Width           =   5295
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   13200
         Picture         =   "FrmBuscarProducto.frx":06DD
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   12435
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   375
         Width           =   3210
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3630
         TabIndex        =   4
         Top             =   375
         Width           =   7380
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   120
         Left            =   3630
         TabIndex        =   6
         Top             =   855
         Width           =   7380
         _ExtentX        =   13018
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   750
         Left            =   11115
         MousePointer    =   99  'Custom
         TabIndex        =   7
         Top             =   225
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   0
         Width           =   3015
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   2250
         X2              =   12250
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   11355
         MouseIcon       =   "FrmBuscarProducto.frx":245F
         MousePointer    =   99  'Custom
         Picture         =   "FrmBuscarProducto.frx":2769
         Stretch         =   -1  'True
         Top             =   300
         Width           =   600
      End
   End
   Begin VB.CommandButton CmdBuscar 
      Appearance      =   0  'Flat
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1090
      Index           =   0
      Left            =   12700
      Picture         =   "FrmBuscarProducto.frx":2815
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   720
      Width           =   1035
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   7050
      LargeChange     =   10
      Left            =   13105
      TabIndex        =   1
      Top             =   2070
      Width           =   675
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   2040
      Width           =   13680
      _ExtentX        =   24130
      _ExtentY        =   12515
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblInfo 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   375
      Left            =   12720
      MousePointer    =   99  'Custom
      TabIndex        =   14
      Top             =   6000
      Width           =   255
   End
   Begin VB.Label lblSelect 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   375
      Left            =   12720
      MousePointer    =   99  'Custom
      TabIndex        =   13
      Top             =   4680
      Width           =   255
   End
End
Attribute VB_Name = "FrmBuscarProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public StrCadBusCod As String
Public StrCadBusDes As String
Public StrOrderBy As String

Public ExistenciaGlobal As Boolean
Public DepositoExistencia As String
Public PrecioCliente As String
Public EvitarActivate As Boolean
Public BusquedaInstantanea As Boolean
Public CustomFontSize As String
Public Vista As FrmBuscarProducto_Vista
Public ArrResultado
Public ProductosNoCompuestos As Boolean

Public Enum FrmBuscarProducto_Vista
    VistaPrecioCliente
    VistaMarca
End Enum

Private ContPuntos As Long, ContItems As Long, ContFound As Long, StrCont As String
Private StrSQL As String
Private ArrCamposBusquedas()
Public Conex_SC As ADODB.Connection
Private Band As Boolean
Private StrSqlMasCondicion As String
Private StrCondicion As String
Private PermisoFichaProductos As Boolean

Private mRowCell As Long, mColCell As Long, Fila As Long

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Function IgnorarActivate() As Boolean
    EvitarActivate = True: IgnorarActivate = EvitarActivate
End Function

Private Sub cmbItemBusqueda_Click()
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
End Sub

Private Sub cmbItemBusqueda_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 49 To 57
            On Error Resume Next
            cmbItemBusqueda.ListIndex = Val(Chr(KeyAscii)) - 1
        Case vbKeyReturn
            If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    End Select
End Sub

Private Sub cmdInfo_Click()
    'MsgBox "info"
    'CargarInfoProductos Grid.TextMatrix(Grid.Row, 0), PermisoFichaProductos, False, False, PermisoFichaProductos, False, False, True, False, False
End Sub

Private Sub CmdSelect_Click()
    'MsgBox "ok"
    grid_DblClick
End Sub

Private Sub CmdTeclado_Click()
    'IgnorarActivate
    'TECLADO.Show vbModal
    TecladoWindows Me.txtDato
End Sub

Private Sub Exit_Click()
    Select Case KeyCode
        Case KeyCode
            ArrResultado = Empty
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Exit_Click
End Sub

Private Sub Form_Load()
    
    Label1.Caption = Stellar_Mensaje(146) 'criterios de busqueda
    CmdBuscar(0).Caption = Stellar_Mensaje(147) 'buscar
    
    With GRID
        .Clear
        .SelectionMode = flexSelectionByRow
        .FixedCols = 0
        .FixedRows = 1
        .Rows = 1
        .Cols = 6
        .RowHeight(0) = 425
        .RowHeightMin = 600 '.RowHeight(0) * 2
        If Vista = VistaPrecioCliente Then
            .FormatString = StellarMensaje(148) & "|" & StellarMensaje(149) & "|" & StellarMensaje(150) & "|" '"Codigo|Descripcion|Precio|"
        ElseIf Vista = VistaMarca Then
            .FormatString = StellarMensaje(148) & "|" & StellarMensaje(149) & "|" & StellarMensaje(151) & "|" '"Codigo|Descripcion|Marca|"
        End If
        .ColWidth(0) = 2000
        .ColAlignment(0) = flexAlignCenterCenter
        .ColWidth(1) = 6580
        .ColAlignment(1) = flexAlignLeftCenter
        .ColWidth(2) = 2000
        If Vista = VistaPrecioCliente Then
            .ColAlignment(2) = flexAlignRightCenter
        ElseIf Vista = VistaMarca Then
            .ColAlignment(2) = flexAlignLeftCenter
        End If
        .ColWidth(3) = 0 '1500
        .ColWidth(4) = 3000 '1500
        .ColWidth(5) = 0 ' Columna sin datos para ocultar la seleccion de columnas en la fila de cabecero fija (0) para que no le cambie el color.
        .ScrollTrack = True
        .Row = 0
        .Col = 5
        .ColSel = 5
    End With
    
    Dim mRs As New ADODB.Recordset
    
    If GRID.Rows > 8 Then
        ScrollGrid.Visible = True
        GRID.ScrollBars = flexScrollBarBoth
    Else
        GRID.ScrollBars = flexScrollBarHorizontal
        ScrollGrid.Visible = False
    End If
    
    If ExistenciaGlobal Then
        StrSQL = "SELECT TOP(200) isNULL(ExGlobal.Cantidad, 0) AS ExistenciaGlobal, Producto.c_Codigo, Producto.c_Descri, Producto.n_Precio1, Producto.n_Precio2, Producto.n_Precio3, Producto.c_Marca, Producto.c_Modelo FROM (" & GetLines & _
        "SELECT SUM(n_Cantidad) AS Cantidad, c_CodArticulo FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_DEPOPROD GROUP BY c_CodArticulo" & GetLines & _
        ") ExGlobal RIGHT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS Prod" & GetLines & _
        "ON ExGlobal.c_CodArticulo = Prod.C_CODIGO"
    Else
        If DepositoExistencia = vbNullString Then DepositoExistencia = DPS_Local
        
        StrSQL = "SELECT TOP (200) isNULL(Existencia.n_Cantidad, 0) AS ExistenciaDeposito, Producto.c_Codigo, Producto.c_Descri, Producto.n_Precio1, Producto.n_Precio2, Producto.n_Precio3, Producto.c_Marca, Producto.c_Modelo" & GetLines & _
        "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS Producto LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_DEPOPROD Existencia" & GetLines & _
        "ON Producto.c_Codigo = Existencia.c_CodArticulo" & GetLines & _
        "AND Existencia.c_CodDeposito = '" & DepositoExistencia & "'"
    End If
    
    cmbItemBusqueda.Clear
    cmbItemBusqueda.AddItem StellarMensaje(149) '"Descripci�n"
    cmbItemBusqueda.AddItem StellarMensaje(148) '"C�digo"
    cmbItemBusqueda.AddItem StellarMensaje(151) '"Marca"
    cmbItemBusqueda.AddItem StellarMensaje(152) '"Modelo"
    cmbItemBusqueda.ListIndex = 0
    
    ArrCamposBusquedas = Array("c_Descri", "c_Codigo", "c_Marca", "c_Modelo")
    
    lblTeclado.MouseIcon = CmdTeclado.MouseIcon
    
'    Call Apertura_RecordsetC(rsMenuUser)
'
'    rsMenuUser.Open "SELECT * FROM CONF_MENU_USER WHERE Clave_User = 'org" & lcClave_User & "' AND Clave_Menu = 'Inv006' AND Activado = 1 ORDER BY ID", _
'    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
'
'    Set rsMenuUser.ActiveConnection = Nothing

    

    'If Not rsMenuUser.EOF Then
        PermisoFichaProductos = True
    'Else
        'grid.ColWidth(3) = 0
    'End If
    
    'If Conex_SC Is Nothing Then Set Conex_SC = frmConfigPlatos.fCls.Conexion
    'If txtDato.Text = vbNullString Then txtDato.Text = "%"
    
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Private Sub grid_DblClick()
    
    If GRID.Rows <= 1 Then Exit Sub
    
    Dim TmpResult
    
    ReDim TmpResult(0 To (GRID.Cols - 1))
    
    For i = 0 To UBound(TmpResult)
        TmpResult(i) = GRID.TextMatrix(GRID.Row, i)
    Next i
    
    ArrResultado = TmpResult
    
    Unload Me
    
 End Sub

Private Sub Form_Unload(Cancel As Integer)
    EvitarActivate = False
    BusquedaInstantanea = False
    CustomFontSize = vbNullString
    StrOrderBy = vbNullString
    StrCriterioBase = vbNullString
    PrecioCliente = vbNullString
End Sub

Private Sub Form_Activate()
    
    If EvitarActivate Then EvitarActivate = False: Exit Sub
    
    Dim Longitud As Long
    
    Call AjustarPantalla(Me)
    
    'Medir.FontName = lblTitulo.FontName
    'Medir.FontSize = lblTitulo.FontSize
    'Medir.FontBold = lblTitulo.FontBold
    
    Me.lbl_Organizacion.Caption = StellarMensaje(144)
    
    'Longitud = Medir.TextWidth(lblTitulo.Caption)
    
    'If Longitud > 0 Then
        'Line1.X1 = Line1.X1 + (Longitud)
    'End If
    
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    
    txtDato.SelStart = Len(txtDato.Text)
            
    If Trim(CustomFontSize) <> vbNullString Then GRID.Font.Size = Val(CustomFontSize)
    
    If PrecioCliente = vbNullString Then
        PrecioCliente = "1"
    Else
        PrecioCliente = ValidarNumeroIntervalo(SVal(PrecioCliente), 3, 1)
    End If
    
    If StrOrderBy = vbNullString Then StrOrderBy = "c_Descri"
    
    If BusquedaInstantanea Then txtDato_KeyPress vbKeyReturn
    
End Sub

Private Sub FrameInfo_Click()
    cmdInfo_Click
End Sub

Private Sub FrameSelect_Click()
    CmdSelect_Click
End Sub

Private Sub Grid_EnterCell()
    
    On Error Resume Next
    
    If GRID.Rows = 1 Then
        FrameSelect.Visible = False
        FrameInfo.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> GRID.Row Then
            
            If Fila > 0 Then
                GRID.RowHeight(Fila) = 600
            End If
            
            GRID.RowHeight(GRID.Row) = 720
            Fila = GRID.Row
            
        End If
    
        MostrarEditorTexto2 Me, GRID, CmdSelect, mRowCell, mColCell
        GRID.ColSel = 0
        
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
    Else
        Band = False
    End If
    
End Sub

Public Sub MostrarEditorTexto2(pFrm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pResp As Boolean = False)
    
    On Error Resume Next
    
    With pGrd

        .RowSel = .Row
        
        If .ColWidth(3) > 10 Then
        
            If .Col <> 3 Then Band = True
            .Col = 3
            
            'CmdInfo.move .Left + .CellLeft + Fix((.CellWidth / 2) - (CmdInfo.Width / 2)), .Top + .CellTop + Fix((.CellHeight / 2) - (CmdInfo.Height / 2))
            'lblInfo.move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
            'CmdInfo.Visible = True: lblInfo.Visible = True
            'CmdInfo.ZOrder 0
            'lblInfo.ZOrder 0
            
            FrameInfo.BackColor = pGrd.BackColorSel
            'FrameInfo.BackColor = pGrd.BackColor
            FrameInfo.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
            CmdInfo.Move ((FrameInfo.Width / 2) - (CmdInfo.Width / 2)), ((FrameInfo.Height / 2) - (CmdInfo.Height / 2))
            FrameInfo.Visible = True: CmdInfo.Visible = True
            FrameInfo.ZOrder
            
        End If
        
        If .Col <> 4 Then Band = True
        .Col = 4
        
        'CmdSelect.move .Left + .CellLeft + Fix((.CellWidth / 2) - (CmdSelect.Width / 2)), .Top + .CellTop + Fix((.CellHeight / 2) - (CmdSelect.Height / 2))
        'lblSelect.move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        'CmdSelect.Visible = True: lblSelect.Visible = True
        'CmdSelect.ZOrder 0
        'lblSelect.ZOrder 0
        
        FrameSelect.BackColor = pGrd.BackColorSel
        'FrameSelect.BackColor = pGrd.BackColor
        FrameSelect.Move .Left + .CellLeft, .Top + .CellTop, .CellWidth, .CellHeight
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        cellRow = .Row
        cellCol = .Col

     End With
     
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> GRID.Row Then
        GRID.TopRow = ScrollGrid.Value
        GRID.Row = ScrollGrid.Value
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub CmdBuscar_Click(Index As Integer)
    Call txtDato_KeyPress(vbKeyReturn)
End Sub

Private Sub txtDato_Click()
    If ModoTouch Then CmdTeclado_Click
End Sub

Private Sub PrepararBusqueda()
        SeleccionarTexto txtDato
End Sub

Private Sub txtDato_GotFocus()
    PrepararBusqueda
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        Exit_Click
    ElseIf KeyCode = vbKeyF3 Then
        If PuedeObtenerFoco(cmbItemBusqueda) Then cmbItemBusqueda.SetFocus
    ElseIf KeyCode = vbKeyF4 Then
        On Error Resume Next
        cmbItemBusqueda.ListIndex = IIf(cmbItemBusqueda.ListIndex <> 0, 0, 1)
    ElseIf KeyCode = vbKeyF8 Then
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
    End If
End Sub

Sub Desactivar_Objetos(tipo As Boolean)
    GRID.Enabled = tipo
    GRID.Visible = tipo
    Me.CmdBuscar(0).Enabled = tipo
End Sub

Public Function Consulta_Mostrar() As Boolean
    
    Dim RsConsulta As New ADODB.Recordset
    Dim Cnx As New ADODB.Connection
    Dim Cont As Long, ContItems As Long
    
    Cont = 0
    
    If Trim(StrSqlMasCondicion) = Empty Then
        Exit Function
    End If
    
    Call Desactivar_Objetos(False)
    
    On Error GoTo GetError
    
    Screen.MousePointer = 11
    
    GRID.Rows = 1
    
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    
    If StrOrderBy <> "" Then
        StrSqlMasCondicion = StrSqlMasCondicion + " ORDER BY " + Me.StrOrderBy
    End If
    
    RsConsulta.CursorLocation = adUseServer
    
    Apertura_Recordset RsConsulta
    
    'Debug.Print StrSqlMasCondicion
    
    RsConsulta.Open StrSqlMasCondicion, Cnx, adOpenKeyset, adLockReadOnly
    
    If Not RsConsulta.EOF Then
        
        RsConsulta.MoveLast
        RsConsulta.MoveFirst
        
        ContFound = 0
        
        Me.Barra_Prg.Min = 0
        Me.Barra_Prg.Value = 0
        'Me.Tim_Progreso.Enabled = True
        Me.Barra_Prg.Max = RsConsulta.RecordCount
        
        ContFound = RsConsulta.RecordCount
        
        i = 0
        
        ScrollGrid.Min = 0
        ScrollGrid.Max = ContFound
        ScrollGrid.Value = 0
        
        Do Until RsConsulta.EOF
            
            DoEvents

            i = i + 1
            
            If i > GRID.Rows - 1 Then GRID.Rows = GRID.Rows + 1
                
            GRID.TextMatrix(i, 0) = isDBNull(RsConsulta!C_CODIGO, vbNullString)
            GRID.TextMatrix(i, 1) = isDBNull(RsConsulta!C_DESCRI, vbNullString)
            
            If Vista = VistaPrecioCliente Then
                GRID.TextMatrix(i, 2) = FormatNumber(isDBNull(RsConsulta.Fields("n_Precio" & PrecioCliente).Value, "0"), 2, vbTrue, vbFalse, vbTrue)
            ElseIf Vista = VistaMarca Then
                GRID.TextMatrix(i, 2) = isDBNull(RsConsulta.Fields("c_Marca").Value, vbNullString)
            End If
            
            Me.Barra_Prg.Value = ContItems
            
            RsConsulta.MoveNext
            
            ContItems = ContItems + 1
            
        Loop
        
        If GRID.Rows > 8 Then
            ScrollGrid.Visible = True
            GRID.ScrollBars = flexScrollBarBoth
            GRID.ColWidth(1) = 5920
        Else
            GRID.ScrollBars = flexScrollBarHorizontal
            ScrollGrid.Visible = False
            GRID.ColWidth(1) = 6580
        End If
        
        Me.Barra_Prg.Value = Me.Barra_Prg.Max
        
        Call Desactivar_Objetos(True)
        
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
        
        Fila = 0
        Grid_EnterCell
        
    Else
        GRID.Col = GRID.Cols - 1
        GRID.ColSel = GRID.Col
        ScrollGrid.Visible = False
        IgnorarActivate
        Mensajes Stellar_Mensaje(145) '"No hay ning�n Item que cumpla con los Par�metros de B�squeda."
    End If
    
    RsConsulta.Close
    
    Screen.MousePointer = 0
    
    Me.Barra_Prg.Value = 0
    
    On Error GoTo 0
    
    Consulta_Mostrar = True
    
    Call Desactivar_Objetos(True)
    
    Fila = 0
    
    Exit Function
    
GetError:
        
    Call Desactivar_Objetos(True)
    
    Screen.MousePointer = 0
    Me.Barra_Prg.Value = 0
    
    Call IgnorarActivate
    Mensajes Err.Description 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
    Consulta_Mostrar = False
    
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    
End Function

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    
    Dim mDatTemp As String
    
    On Error GoTo GetError
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            
            mDatTemp = txtDato.Text
                            
            'If Trim(txtDato) = vbNullString Then
                'If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
                'Exit Sub
            'End If
            
            If Trim(txtDato) = "" Then txtDato.Text = "%"
            
            Dim WhereProductosNoCompuestos As String
            WhereProductosNoCompuestos = "n_TipoPeso <> 5"

            If txtDato = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
                StrSqlMasCondicion = StrSQL
                
                If Not ProductosNoCompuestos Then
                    StrSqlMasCondicion = StrSqlMasCondicion & " WHERE " & WhereProductosNoCompuestos
                End If
                
                Consulta_Mostrar
                txtDato.Text = mDatTemp
                PrepararBusqueda
                
            Else
                
                StrSqlMasCondicion = StrSQL
                
                StrCadBusDes = ArrCamposBusquedas(Me.cmbItemBusqueda.ListIndex)
                
                If UCase(StrCadBusDes) = UCase("c_Codigo") Then
                    
                    StrCondicion = GetLines & "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_CODIGOS Codigos " & GetLines & _
                    "ON Producto.c_Codigo = Codigos.c_CodNasa" & GetLines & _
                    "WHERE Codigos.c_Codigo LIKE '" & Me.txtDato & "%'"
                                                                                                                                                
                    StrSqlMasCondicion = StrSQL + StrCondicion
                                       
                Else
                
                    If InStr(UCase(StrSqlMasCondicion), "WHERE") Then
                        StrCondicion = " AND " & StrCadBusDes & " LIKE '" & Me.txtDato & "%'"
                    Else
                        StrCondicion = " WHERE  " & StrCadBusDes & " LIKE '" & Me.txtDato & "%' "
                    End If

                    If Not ProductosNoCompuestos Then
                        StrCondicion = StrCondicion & " AND " & WhereProductosNoCompuestos
                    End If

                    StrSqlMasCondicion = StrSQL & StrCondicion
                    
                End If
                
                Consulta_Mostrar
                txtDato.Text = mDatTemp
                PrepararBusqueda
                
            End If
            
    End Select
    
    Screen.MousePointer = 0
    
    On Error GoTo 0
    
    Exit Sub
    
GetError:
    
    Call IgnorarActivate
    Mensajes Err.Description 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
End Sub
