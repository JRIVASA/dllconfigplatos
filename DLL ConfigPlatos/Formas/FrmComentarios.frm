VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmComentarios 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8475
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8475
   ScaleWidth      =   15315
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmd_QuitarRelacion 
      Caption         =   "<-----"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7320
      TabIndex        =   12
      Top             =   5160
      Width           =   855
   End
   Begin VB.CommandButton cmd_AgregarRelacion 
      Caption         =   "----->"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7320
      TabIndex        =   11
      Top             =   4320
      Width           =   855
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   5775
      Left            =   8280
      TabIndex        =   8
      Top             =   2400
      Width           =   6855
      Begin VB.CommandButton cmd_Buscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   5640
         TabIndex        =   16
         Top             =   5040
         Visible         =   0   'False
         Width           =   1095
      End
      Begin MSComctlLib.ListView ListComentariosPlato 
         Height          =   4335
         Left            =   240
         TabIndex        =   14
         Top             =   600
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   7646
         View            =   2
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin VB.Label lblComentario 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Comentarios Asociados Al Plato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   10
         Top             =   120
         Width           =   3495
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   5775
      Left            =   240
      TabIndex        =   7
      Top             =   2400
      Width           =   6855
      Begin VB.CommandButton cmdUbicar 
         Appearance      =   0  'Flat
         Caption         =   "Ubicar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4590
         TabIndex        =   18
         Top             =   5040
         Width           =   855
      End
      Begin VB.TextBox txtUbicar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   240
         TabIndex        =   17
         Top             =   5160
         Width           =   4215
      End
      Begin VB.CommandButton cmd_EliminarComentario 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   5640
         TabIndex        =   15
         Top             =   5040
         Width           =   1095
      End
      Begin MSComctlLib.ListView listComentariosDisponibles 
         Height          =   4335
         Left            =   240
         TabIndex        =   13
         Top             =   600
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   7646
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin VB.Label lblComentario 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Comentarios Disponibles:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   120
         Width           =   2535
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Height          =   1335
      Left            =   240
      TabIndex        =   3
      Top             =   720
      Width           =   14895
      Begin VB.CommandButton cmd_AgregarComentario 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   13440
         TabIndex        =   6
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox txtComentario 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   495
         Left            =   240
         TabIndex        =   4
         Top             =   480
         Width           =   12975
      End
      Begin VB.Label lblComentario 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Comentario:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   120
         Width           =   1575
      End
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12315
         TabIndex        =   2
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuracion de Comentarios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   75
         Width           =   5295
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "FrmComentarios.frx":0000
         Top             =   0
         Width           =   480
      End
   End
End
Attribute VB_Name = "FrmComentarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public mConexion As Object
Public mCodigoPlato As String
Public mCodigoProducto As String
Private RsComentarios As New ADODB.Recordset
Private RsComentariosPlato As New ADODB.Recordset

Private Sub cmd_AgregarComentario_Click()
    
    Dim Comentario As String
    Dim mSQl As String
    
    If Not Trim(txtComentario.Text) = Empty Then
        
        Comentario = txtComentario.Text
        
        mSQl = "INSERT INTO MA_COMENTARIOS (cs_Comentario) " & _
        "VALUES ('" & FrmAppLink.FixTSQL(Comentario) & "')"
        
        mConexion.Execute mSQl, TmpRecAffec
        
        listComentariosDisponibles.ListItems.Clear
       
        Call ArmarListadoComentariosDisponibles
        
        txtComentario.Text = Empty
        
    Else
        
        Mensajes "El texto del comentario no debe estar vacio. ", False
        
    End If
    
End Sub

Private Sub cmd_AgregarRelacion_Click()
    
    'mCodigoPlato
    
    Dim mSQl As String
    
    If listComentariosDisponibles.ListItems.Count > 0 Then
        
        IdComentario = listComentariosDisponibles.SelectedItem.ListSubItems(1)
        
        mSQl = "INSERT INTO MA_PLATO_COMENTARIO (ID_PLATO, ID_COMENTARIO, OBLIGATORIO) " & _
        "VALUES (" & mCodigoPlato & "," & IdComentario & ",0)"
        
        mConexion.Execute mSQl, TmpRecAffec
        
        listComentariosDisponibles.ListItems.Remove (listComentariosDisponibles.SelectedItem.Index)
        ListComentariosPlato.ListItems.Clear
        
        Call ArmarListadoComentariosPlatos
        
    End If
    
End Sub

Private Sub cmd_EliminarComentario_Click()
    
    Dim mSQl As String
    
    If listComentariosDisponibles.ListItems.Count > 0 Then
        
        IdComentario = listComentariosDisponibles.SelectedItem.ListSubItems(1)
        
        mSQl = "DELETE FROM MA_COMENTARIOS WHERE ID_COMENTARIO = " & IdComentario & ""
        
        mConexion.Execute mSQl, TmpRecAffec
        
        listComentariosDisponibles.ListItems.Remove (listComentariosDisponibles.SelectedItem.Index)
        
    End If
    
End Sub

Private Sub cmd_QuitarRelacion_Click()
    
    Dim mSQl As String
    
    If ListComentariosPlato.ListItems.Count > 0 Then
        
        IdComentario = ListComentariosPlato.SelectedItem.SubItems(1)
        
        mSQl = "DELETE FROM MA_PLATO_COMENTARIO " & _
        "WHERE Id_plato = " & mCodigoPlato & " " & _
        "AND ID_COmentario = " & IdComentario & " "
        
        mConexion.Execute mSQl, TmpRecAffec
        
        ListComentariosPlato.ListItems.Remove ListComentariosPlato.SelectedItem.Index
        
        listComentariosDisponibles.ListItems.Clear
        
        Call ArmarListadoComentariosDisponibles
        
    End If
    
End Sub

Private Sub cmdUbicar_Click()
    
    Dim Item As ListItem
    Dim Opcion As Integer
    Dim Texto As String
    
    Texto = UCase(Trim(txtUbicar.Text))
    Opcion = lvwText
    
    For i = 1 To listComentariosDisponibles.ListItems.Count
        If InStr(UCase(Trim(listComentariosDisponibles.ListItems(i).Text)), Texto) > 0 Then
            Set Item = listComentariosDisponibles.ListItems(i)
        End If
    Next

    If Item Is Nothing Then
        Mensajes "No existen Coincidencias", False
    Else
        Item.EnsureVisible
        Item.Selected = True
        listComentariosDisponibles.SetFocus
    End If
    
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    ListComentariosPlato.View = lvwReport
    ListComentariosPlato.LabelEdit = lvwManual
End Sub

Private Sub Form_Load()
    
    'cargamos los datos de los registros
    'Dim RsComentarios As New ADODB.Recordset
    Call ArmarListadoComentariosDisponibles
    
    Call ArmarListadoComentariosPlatos
    
    'listComentariosDisponibles.View = lvwList
    'ListComentariosPlato.View = lvwList
    
End Sub

Private Sub ArmarListadoComentariosDisponibles()
    
    Dim mSQl As String
    Dim ActItem As ListItem
    
    mSQl = "SELECT * FROM MA_COMENTARIOS " & _
    "WHERE Id_Comentario NOT IN " & _
    "(SELECT Id_Comentario FROM MA_PLATO_COMENTARIO " & _
    "WHERE Id_Plato = '" & mCodigoPlato & "') "
    
    RsComentarios.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    listComentariosDisponibles.GridLines = True
    listComentariosDisponibles.HideColumnHeaders = False
    
    X = listComentariosDisponibles.Width
    
    listComentariosDisponibles.ColumnHeaders.Add 1, , "Descripción", listComentariosDisponibles.Width
    listComentariosDisponibles.ColumnHeaders.Add 2, , "Codigo", 1000
    
    If Not RsComentarios.EOF Then
        
        While Not RsComentarios.EOF
            
            Set ActItem = listComentariosDisponibles.ListItems.Add(, , RsComentarios!cs_Comentario)
            'listComentariosDisponibles.ListItems.Item .SubItems(1) = RsComentarios!cs_Comentario
            ActItem.SubItems(1) = RsComentarios!ID_Comentario
            
            RsComentarios.MoveNext
            
        Wend
        
    End If
    
    RsComentarios.Close
    
End Sub

Private Sub ArmarListadoComentariosPlatos()
    
    Dim mSQl As String
    Dim ActItem As ListItem
    
    mSQl = "SELECT pc.*, c.cs_comentario " & _
    "From ma_comentarios c, ma_plato_comentario pc " & _
    "Where pc.Id_comentario = c.Id_comentario AND pc.ID_PLATO = '" & mCodigoPlato & "'"
    
    RsComentariosPlato.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ListComentariosPlato.ColumnHeaders.Add 1, , "Descripción", ListComentariosPlato.Width
    ListComentariosPlato.ColumnHeaders.Add 2, , "IDComentario", 0
    ListComentariosPlato.ColumnHeaders.Add 3, , "Obligatorio", 0
    ListComentariosPlato.ColumnHeaders.Add 4, , "Comentario", 0
    
    If Not RsComentariosPlato.EOF Then
        
        While Not RsComentariosPlato.EOF
            
            Set ActItem = ListComentariosPlato.ListItems.Add(, , RsComentariosPlato!cs_Comentario)
            
'            ListComentariosPlato.ListItems.Item.SubItems(1) = RsComentariosPlato!ID_Comentario
'            ListComentariosPlato.ListItems.Item.SubItems(2) = RsComentariosPlato!Obligatorio
'            ListComentariosPlato.ListItems.Item.SubItems(3) = RsComentariosPlato!cs_Comentario
            ActItem.SubItems(1) = RsComentariosPlato!ID_Comentario
            ActItem.SubItems(2) = RsComentariosPlato!Obligatorio
            ActItem.SubItems(3) = RsComentariosPlato!ID_Plato
            
            RsComentariosPlato.MoveNext
            
        Wend
        
    End If
    
    RsComentariosPlato.Close
    
End Sub

Private Sub listComentariosDisponibles_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    Dim Col As Long
    
    Col = ColumnHeader.Index - 1
    
    If listComentariosDisponibles.SortOrder = lvwAscending Then
        listComentariosDisponibles.SortOrder = lvwDescending
    Else
        listComentariosDisponibles.SortOrder = lvwAscending
    End If
    
    listComentariosDisponibles.SortKey = Col
    listComentariosDisponibles.Sorted = True
    
End Sub
