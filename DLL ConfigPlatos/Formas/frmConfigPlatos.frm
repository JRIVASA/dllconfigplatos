VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frmConfigPlatos 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "frmConfigPlatos.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   3375
      Left            =   11040
      TabIndex        =   34
      Top             =   1680
      Width           =   3975
      Begin VB.CheckBox chkComentario 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Usa Comentario Obligatorio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   480
         TabIndex        =   52
         Top             =   2880
         Width           =   2895
      End
      Begin VB.TextBox TxtTotalInformativo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   47
         Top             =   1680
         Width           =   2895
      End
      Begin VB.ComboBox cboImpuesto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   480
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   2400
         Width           =   2895
      End
      Begin VB.TextBox txtPrecio 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   480
         TabIndex        =   36
         Top             =   960
         Width           =   2895
      End
      Begin VB.TextBox Txtcosto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   240
         Width           =   2895
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   7
         Left            =   480
         TabIndex        =   46
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Impuesto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   6
         Left            =   480
         TabIndex        =   40
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Precio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   5
         Left            =   480
         TabIndex        =   39
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   4
         Left            =   480
         TabIndex        =   38
         Top             =   0
         Width           =   1215
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15585
      TabIndex        =   32
      Top             =   480
      Width           =   15615
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   33
         Top             =   120
         Width           =   12090
         _ExtentX        =   21325
         _ExtentY        =   1429
         ButtonWidth     =   1905
         ButtonHeight    =   1429
         Style           =   1
         ImageList       =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   8
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Nuevo Plato"
               Key             =   "nuevo"
               Object.ToolTipText     =   "Crear Plato, Tecla F3"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Buscar"
               Key             =   "modificar"
               Object.ToolTipText     =   "Modicar un plato Existente, Tecla F2"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Grabar"
               Key             =   "grabar"
               Object.ToolTipText     =   "Graba Configuracion, Tecla F4"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Cancelar"
               Key             =   "cancelar"
               Object.ToolTipText     =   "Cancelar Operacion, Tecla F7"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salir"
               Key             =   "salir"
               Object.ToolTipText     =   "Tecla F12"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Importar"
               Key             =   "config"
               Object.ToolTipText     =   "Importar Configuracion, Tecla F10"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               Object.Tag             =   "Teclado"
               ImageIndex      =   10
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14640
         Picture         =   "frmConfigPlatos.frx":000C
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Configuracion de Platos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12315
         TabIndex        =   30
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame fraPlato 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Producto"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   3375
      Left            =   240
      TabIndex        =   16
      Top             =   1680
      Width           =   10455
      Begin VB.CommandButton cmdCategoria 
         Height          =   360
         Index           =   3
         Left            =   2280
         Picture         =   "frmConfigPlatos.frx":1D8E
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   2910
         Width           =   420
      End
      Begin VB.TextBox txtCategoria 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   3
         Left            =   480
         Locked          =   -1  'True
         TabIndex        =   48
         Top             =   2910
         Width           =   1695
      End
      Begin VB.TextBox txtProducto 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   480
         TabIndex        =   0
         Top             =   240
         Width           =   9255
      End
      Begin VB.TextBox txtCategoria 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   0
         Left            =   480
         TabIndex        =   1
         Top             =   900
         Width           =   1695
      End
      Begin VB.CommandButton cmdCategoria 
         Height          =   360
         Index           =   0
         Left            =   2280
         Picture         =   "frmConfigPlatos.frx":2590
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   900
         Width           =   420
      End
      Begin VB.TextBox txtCategoria 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   1
         Left            =   480
         TabIndex        =   2
         Top             =   1560
         Width           =   1695
      End
      Begin VB.TextBox txtCategoria 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   2
         Left            =   480
         TabIndex        =   3
         Top             =   2280
         Width           =   1695
      End
      Begin VB.CommandButton cmdCategoria 
         Height          =   360
         Index           =   1
         Left            =   2280
         Picture         =   "frmConfigPlatos.frx":2D92
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   1560
         Width           =   420
      End
      Begin VB.CommandButton cmdCategoria 
         Height          =   360
         Index           =   2
         Left            =   2280
         Picture         =   "frmConfigPlatos.frx":3594
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   2280
         Width           =   420
      End
      Begin VB.Label lblCategoria 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   3
         Left            =   2880
         TabIndex        =   51
         Top             =   2910
         Width           =   6855
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Moneda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   8
         Left            =   480
         TabIndex        =   50
         Top             =   2700
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Producto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   26
         Top             =   0
         Width           =   735
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   25
         Top             =   680
         Width           =   1455
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   24
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "SubGrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   23
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label lblCategoria 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   0
         Left            =   2880
         TabIndex        =   22
         Top             =   900
         Width           =   6855
      End
      Begin VB.Label lblCategoria 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   1
         Left            =   2880
         TabIndex        =   21
         Top             =   1560
         Width           =   6855
      End
      Begin VB.Label lblCategoria 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Index           =   2
         Left            =   2880
         TabIndex        =   20
         Top             =   2280
         Width           =   6855
      End
   End
   Begin VB.CommandButton cmdDel 
      Height          =   615
      Left            =   14280
      Picture         =   "frmConfigPlatos.frx":3D96
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   5835
      Width           =   615
   End
   Begin VB.CommandButton cmdAdd 
      Height          =   615
      Left            =   13560
      Picture         =   "frmConfigPlatos.frx":99A8
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   5835
      Width           =   615
   End
   Begin VB.Frame fraGrupo 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   240
      TabIndex        =   10
      Top             =   5760
      Width           =   14775
      Begin VB.CommandButton Command1 
         Height          =   480
         Left            =   3840
         Picture         =   "frmConfigPlatos.frx":A672
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   360
         Width           =   540
      End
      Begin VB.CheckBox chksistema 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Sistema"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   4800
         TabIndex        =   28
         Top             =   420
         Width           =   1215
      End
      Begin VB.CheckBox chkRequerido 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Requerido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   8400
         TabIndex        =   27
         Top             =   420
         Width           =   1215
      End
      Begin VB.TextBox txtGrupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   405
         Left            =   360
         MaxLength       =   50
         TabIndex        =   4
         Top             =   375
         Width           =   3375
      End
      Begin VB.ComboBox cboCantidad 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   10320
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   420
         Width           =   1215
      End
      Begin VB.CheckBox chkCosto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Costeable"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   6480
         TabIndex        =   5
         Top             =   420
         Width           =   1335
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Cantidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10680
         TabIndex        =   11
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame frmItems 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3735
      Left            =   240
      TabIndex        =   9
      Top             =   6720
      Width           =   14775
      Begin VB.Frame FrameTiempoPrep 
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Falta eliminar plato"
         Height          =   495
         Left            =   12120
         TabIndex        =   55
         Top             =   2640
         Width           =   2055
         Begin VB.Label LblTiempoPrep 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "T. Preparacion"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   240
            TabIndex        =   56
            Top             =   120
            Width           =   1455
         End
      End
      Begin VB.Frame FrmComentario 
         BackColor       =   &H00AE5B00&
         BorderStyle     =   0  'None
         Caption         =   "Falta eliminar plato"
         Height          =   495
         Left            =   12120
         TabIndex        =   53
         Top             =   1920
         Width           =   2055
         Begin VB.Label lblComentario 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   240
            TabIndex        =   54
            Top             =   105
            Width           =   1455
         End
      End
      Begin VB.Frame Frame3 
         BackColor       =   &H00808080&
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   495
         Left            =   12120
         TabIndex        =   43
         Top             =   1200
         Width           =   2055
         Begin VB.Label Eliminar 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   360
            TabIndex        =   45
            Top             =   120
            Width           =   1335
         End
      End
      Begin VB.Frame FrmAgregar 
         BackColor       =   &H0000C000&
         BorderStyle     =   0  'None
         Caption         =   "Falta eliminar plato"
         Height          =   495
         Left            =   12120
         TabIndex        =   42
         Top             =   480
         Width           =   2055
         Begin VB.Label Agregar 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Agregar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   360
            TabIndex        =   44
            Top             =   105
            Width           =   1455
         End
      End
      Begin VB.TextBox txtEdit 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2400
         TabIndex        =   13
         Top             =   2280
         Visible         =   0   'False
         Width           =   1095
      End
      Begin MSFlexGridLib.MSFlexGrid grid 
         Height          =   3495
         Left            =   240
         TabIndex        =   7
         Top             =   120
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   6165
         _Version        =   393216
         FixedCols       =   0
         RowHeightMin    =   360
         ForeColor       =   5790296
         BackColorFixed  =   11426560
         ForeColorFixed  =   16448250
         BackColorSel    =   -2147483625
         ForeColorSel    =   11426560
         BackColorBkg    =   -2147483643
         GridColorFixed  =   -2147483643
         GridLines       =   0
         GridLinesFixed  =   0
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   13800
         MouseIcon       =   "frmConfigPlatos.frx":AE74
         MousePointer    =   99  'Custom
         Picture         =   "frmConfigPlatos.frx":B17E
         Stretch         =   -1  'True
         Top             =   -360
         Visible         =   0   'False
         Width           =   600
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8760
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":B22A
            Key             =   "nuevo"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":B7C4
            Key             =   "modificar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":BD5E
            Key             =   "salir"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":C2F8
            Key             =   "grabar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":C892
            Key             =   "cancelar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":CE2C
            Key             =   "buscar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":D3C6
            Key             =   "config"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5415
      Left            =   120
      TabIndex        =   8
      Top             =   5280
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   9551
      TabWidthStyle   =   2
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   11040
      Top             =   1080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":D960
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":F6F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":103CC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":110A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":12E38
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":14BCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":1695C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":186EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":1A480
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfigPlatos.frx":1C212
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConfigPlatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum eEstadoToolBar
    etbStandard
    etbEdit
End Enum

Public fCls                                                 As clsConfigPlatos

Private mGrupoSel                                           As clsGrupoPlato
Private mItemSel                                            As clsItemGrupo
Private mRowCell                                            As Long
Private mColCell                                            As Long
Private mEstadoToolBar                                      As eEstadoToolBar

Private CodTemp                                             As String
Private MinutosPreparacion                                  As Double

Private Sub cboImpuesto_Click()
    MostrarTotal
End Sub

Private Sub FrameTiempoPrep_DragDrop(Source As Control, X As Single, Y As Single)
    Call LblTiempoPrep_Click
End Sub

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub FrmComentario_DragDrop(Source As Control, X As Single, Y As Single)
    Call lblComentario_Click
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub Agregar_Click()
    grid_KeyDown vbKeyF2, 0
End Sub

Private Sub cboCantidad_Click()
    If Not mGrupoSel Is Nothing Then
        mGrupoSel.Cantidad = cboCantidad.ItemData(cboCantidad.ListIndex)
    End If
End Sub

Private Sub chkCosto_Click()
    mGrupoSel.Costeable = (chkCosto.Value = vbChecked)
    If mGrupoSel.Sistema And mGrupoSel.Costeable Then
        TotalizarPrecio
        chkRequerido.Value = vbChecked
    ElseIf mGrupoSel.Sistema And Not mGrupoSel.Costeable Then
        txtPrecio.Text = FormatNumber(0, sDecMoneda)
    End If
End Sub

Private Sub chkRequerido_Click()
    If Not mGrupoSel Is Nothing Then
        mGrupoSel.Requerido = (chkRequerido.Value = vbChecked)
        cboCantidad.Enabled = mGrupoSel.Requerido
    End If
End Sub

Private Sub cmdAdd_Click()
    AgregarGrupo
End Sub

Private Sub cmdCategoria_Click(Index As Integer)
    
    Select Case Index
        Case 0, 3
            If Index = 0 Then
                CodTemp = txtCategoria(Index).Text
            End If
            BusquedaCategoria Index
        Case 1
            CodTemp = txtCategoria(Index).Text
            If Not ValidarTxtCategoria(Index) Then
                Exit Sub
            End If
            BusquedaCategoria Index 'txtCategoria(0).Text)
        Case 2
            CodTemp = txtCategoria(Index).Text
            If Not ValidarTxtCategoria(Index) Then Exit Sub
            BusquedaCategoria Index 'txtCategoria(0).Text, txtCategoria(1).Text)
        Case Else
            'BusquedaCategoria (Index)
    End Select
    
    If Not IsEmpty(Frm_Super_Consultas.ArrResultado) Then
        
        If Trim(UCase(Frm_Super_Consultas.ArrResultado(0))) <> Empty Then
        
        If Trim(UCase(Frm_Super_Consultas.ArrResultado(0))) <> Trim(UCase(Me.txtCategoria(Index).Text)) Then
            
            If Index < 3 Or (Index = 3 And GRID.TextMatrix(1, 0) = Empty) Then
                
                txtCategoria(Index).Text = Trim(Frm_Super_Consultas.ArrResultado(0))
                Me.lblCategoria(Index).Caption = Trim(Frm_Super_Consultas.ArrResultado(1))
                
                If Index = 0 Or Index = 1 Or Index = 2 Then
                    CodTemp = Empty
                    txtCategoria_LostFocus Index
                End If
                
                If Index = 3 Then
                    
                    Dim mRs As ADODB.Recordset
                    Dim mSQl As String
                    
                    mSQl = "SELECT n_Factor, n_Decimales " & _
                    "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS " & _
                    "WHERE c_CodMoneda = '" & txtCategoria(Index).Text & "' "
                    
                    Set mRs = New ADODB.Recordset
                    
                    mRs.Open mSQl, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
                    
                    If Not mRs.EOF Then
                        sDecMoneda = mRs!n_Decimales
                        sFactor = CDec(mRs!n_Factor)
                    Else
                        sDecMoneda = 2
                        sFactor = CDec(1)
                    End If
                    
                End If
                
            End If
    
            'If Index < 2 Then txtCategoria(Index + 1).SetFocus
        End If
        
        End If
        
    End If
    
End Sub

Private Sub cmdDel_Click()
    If TabStrip1.Tabs.Count > 0 Then
        fCls.Plato.Grupos.Remove TabStrip1.SelectedItem.Index
        TabStrip1.Tabs.Remove TabStrip1.SelectedItem.Index
        TabStrip1_Click
    End If
End Sub

Private Sub CmdTeclado_Click()
    On Error Resume Next
    Call TecladoWindows
End Sub

Private Sub Command2_Click()
    Dim a As Variant
    a = BuscarInfoProducto_Basica(fCls.Conexion, , , , , VistaMarca)
End Sub

Private Sub Command1_Click()
    'grid_KeyDown vbKeyF2,
End Sub

Private Sub Eliminar_Click()
    grid_KeyDown vbKeyDelete, 0
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF2
            If ValidarBoton("modificar") Then BuscarPlato
        Case vbKeyF3
            If ValidarBoton("nuevo") Then NuevoPlato: Me.cmdAdd.Enabled = True
        Case vbKeyF4
            If ValidarBoton("grabar") Then GrabarPlato
        Case vbKeyF7
            If ValidarBoton("cancelar") Then Cancelar: Me.cmdAdd.Enabled = False
        Case vbKeyF10
            If ValidarBoton("config") Then ImportarConfigPlato
        Case vbKeyF12
            Unload Me
        Case vbKeyReturn
            If Not Me.ActiveControl Is Nothing Then
                If Not ActiveControl.Name = GRID.Name Then
                    SendKeys Chr(vbKeyTab)
                End If
            End If
    End Select
    
End Sub

Private Sub Form_Load()
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    lbl_Organizacion.Caption = Stellar_Mensaje(128)
    
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(115)
    Toolbar1.Buttons(2).Caption = Stellar_Mensaje(116)
    Toolbar1.Buttons(3).Caption = Stellar_Mensaje(117)
    Toolbar1.Buttons(4).Caption = Stellar_Mensaje(118)
    Toolbar1.Buttons(6).Caption = Stellar_Mensaje(119)
    Toolbar1.Buttons(7).Caption = Stellar_Mensaje(120)
    Toolbar1.Buttons(8).Caption = Stellar_Mensaje(168)

    Label1(0).Caption = Stellar_Mensaje(103)
    Label1(1).Caption = Stellar_Mensaje(104)
    Label1(2).Caption = Stellar_Mensaje(105)
    Label1(3).Caption = Stellar_Mensaje(106)
    Label1(4).Caption = Stellar_Mensaje(107)
    Label1(5).Caption = Stellar_Mensaje(108)
    Label1(6).Caption = Stellar_Mensaje(109)
    Label1(7).Caption = Stellar_Mensaje(165)
    Label1(8).Caption = Stellar_Mensaje(166)
    
    Label2.Caption = Stellar_Mensaje(110)
    chksistema.Caption = Stellar_Mensaje(111)
    chkCosto.Caption = Stellar_Mensaje(112)
    chkRequerido.Caption = Stellar_Mensaje(113)
    Label3.Caption = Stellar_Mensaje(114)
    Agregar.Caption = Stellar_Mensaje(115)
    Eliminar.Caption = Stellar_Mensaje(169)
    
    MinutosPreparacion = 0
    
    Cancelar
    
    Me.cmdAdd.Enabled = False
    
    mSQl = "SELECT * FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS WHERE b_Preferencia = 1 "
    
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        txtCategoria(3).Text = mRs!c_CodMoneda
        lblCategoria(3).Caption = mRs!c_Descripcion
        sFactor = CDec(mRs!n_Factor)
        sDecMoneda = mRs!n_Decimales
    Else
        sFactor = CDec(1)
        sDecMoneda = 2
    End If
    
    txtGrupo.MaxLength = CampoLength("c_Descripcion", "MA_PLATOS_GRUPOS", fCls.Conexion, Alfanumerico, txtGrupo.MaxLength)
    
    mAplicaTRPend = ManejaSucursales(fCls.Conexion)
    
End Sub

Private Sub Frame3_DragDrop(Source As Control, X As Single, Y As Single)
    grid_KeyDown vbKeyDelete, 0
End Sub

Private Sub FrmAgregar_DragDrop(Source As Control, X As Single, Y As Single)
    grid_KeyDown vbKeyF2, 0
End Sub

Private Sub grid_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyDelete
            If GRID.Col = 0 Then
                EliminarItem GRID.Row
            End If
        Case vbKeyF2
            If GRID.Col = 0 Then
                BuscarItem GRID.Row
                GRID.Col = 1
                grid_KeyPress vbKeyReturn
                txtEdit_LostFocus
            ElseIf GRID.Col > 0 Then
                GRID.Col = 1
                grid_KeyPress vbKeyReturn
                txtEdit_LostFocus
            End If
    End Select
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    
    On Error Resume Next
    
    If GRID.Col = 0 Then
        MostrarEditorTexto Me, GRID, txtEdit, mRowCell, mColCell, KeyAscii, False
    Else
        Set mItemSel = Nothing
        Set mItemSel = mGrupoSel.Items(GRID.Row)
        If Not mItemSel Is Nothing Then
            MostrarEditorTexto Me, GRID, txtEdit, mRowCell, mColCell, KeyAscii, True
        End If
    End If
    
    Err.Clear
    
End Sub

Private Sub grid_DblClick()
    
    Set mItemSel = Nothing
    Set mItemSel = mGrupoSel.Items(GRID.Row)
    
    If Not mItemSel Is Nothing Then
        If GRID.Col = 2 Then
            
            Dim NuevoPrecio As String
            
            NuevoPrecio = InputBoxNumerico(StellarMensaje(163), , False) ' Ingrese el Precio
            
            If Not IsNumeric(NuevoPrecio) Then
                txtEdit.Text = Empty
            Else
                MostrarEditorTexto Me, GRID, txtEdit, mRowCell, mColCell, 0, True
                txtEdit.Text = NuevoPrecio
                txtEdit_LostFocus
            End If
        End If
    End If
    
End Sub

Private Sub lblComentario_Click()
    FrmComentarios.mCodigoPlato = fCls.Plato.CodigoPlato
    FrmComentarios.mCodigoProducto = fCls.Plato.CodigoProducto
    Set FrmComentarios.mConexion = fCls.Conexion
    FrmComentarios.Show vbModal
End Sub

Private Sub LblTiempoPrep_Click()
    
    'llamar quick input box para guardar el dato
    
    Dim Retorno As Double
    
    Retorno = SVal(InputBoxNumerico("Ingrese los Minutos", False, True, MinutosPreparacion, True))
    
    If Retorno > -1 Then
        MinutosPreparacion = Retorno
    End If
    
End Sub

Private Sub TabStrip1_Click()
    If Not TabStrip1.SelectedItem Is Nothing Then
        GRID.Row = 1
        LlenarGrupo TabStrip1.SelectedItem.Index
    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "nuevo"
            Form_KeyDown vbKeyF3, 0
        Case "modificar"
            Form_KeyDown vbKeyF2, 0
        Case "cancelar"
            Form_KeyDown vbKeyF7, 0
        Case "grabar"
            Form_KeyDown vbKeyF4, 0
        Case "config"
            Form_KeyDown vbKeyF10, 0
        Case "salir"
            Form_KeyDown vbKeyF12, 0
        Case "Teclado"
            CmdTeclado_Click
    End Select
End Sub

Private Sub txtCategoria_GotFocus(Index As Integer)
    Select Case Index
        Case 0, 1, 2
            CodTemp = Me.txtCategoria(Index).Text
        Case 3
    End Select
End Sub

Private Sub txtCategoria_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        cmdCategoria_Click Index
    ElseIf KeyCode = vbKeyDelete Then
        If Index <> 3 Then
            txtCategoria(Index).Text = Empty
            txtCategoria_LostFocus Index
        End If
    End If
End Sub

Private Sub txtCategoria_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = Asc("'") Then KeyAscii = 0
End Sub

Private Sub txtCategoria_LostFocus(Index As Integer)
    
    If Index = 3 Then
        Exit Sub
    End If
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    If Trim(txtCategoria(Index).Text) <> Empty Then
        
        If Not ValidarTxtCategoria(Index) Then
            
            LimpiarTxtCategoria Index
            Exit Sub
            
        Else
            
            If UCase(txtCategoria(Index).Text) <> UCase(CodTemp) Then
                
                Set mRs = New ADODB.Recordset
                
                mSQl = BuscarSqlCategoria(Index, False)
                
                ' MsgBox mSql
                
                mRs.Open mSQl, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
                
                If Not mRs.EOF Then
                    Me.lblCategoria(Index).Caption = mRs!Descripcion
                    LimpiarTxtCategoria Index + 1
                Else
                    LimpiarTxtCategoria Index
                End If
                
            End If
            
            CodTemp = Empty
            
        End If
        
    Else
        LimpiarTxtCategoria Index
    End If
    
End Sub

Private Sub Txtcosto_GotFocus()
    ObtenerFoco Txtcosto
End Sub

Private Sub Txtcosto_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidarKeyAsciiNumerico(Txtcosto, KeyAscii, True)
End Sub

Private Sub Txtcosto_LostFocus()
    If Not IsNumeric(Txtcosto.Text) Then Txtcosto.Text = 0
    Txtcosto.Text = FormatNumber(Txtcosto.Text, sDecMoneda)
End Sub

Private Sub txtEdit_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        txtEdit.Visible = False
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    If mColCell = 1 Then
        If Not mItemSel Is Nothing Then
            KeyAscii = ValidarKeyAsciiNumerico(txtEdit, KeyAscii, mItemSel.NumeroDec > 0)
        Else
            KeyAscii = ValidarKeyAsciiNumerico(txtEdit, KeyAscii, False)
        End If
    End If
End Sub

Private Sub txtEdit_LostFocus()
    If txtEdit.Visible Then
        txtEdit.Visible = False
        Select Case mColCell
            Case 0
                BuscarDatosItem txtEdit, mRowCell
            Case 1
                If Not mItemSel Is Nothing Then
                    
                    If IsNumeric(txtEdit.Text) Then mItemSel.Cantidad = CDbl(txtEdit.Text)
                    
                    GRID.TextMatrix(mRowCell, 1) = mItemSel.Cantidad
                    
                    If GRID.Rows - 1 = mRowCell Then
                        GRID.Rows = GRID.Rows + 1
                        GRID.Row = GRID.Rows - 1
                        GRID.Col = 0
                    End If
                    
                    TotalizarCosto
                    
                    If mGrupoSel.Sistema And mGrupoSel.Costeable Then
                        TotalizarPrecio
                    End If
                    
                    If PuedeObtenerFoco(GRID) Then GRID.SetFocus
                    
                End If
            Case 2
                If Not mItemSel Is Nothing Then
                    
                    If IsNumeric(txtEdit.Text) Then mItemSel.Precio = CDbl(txtEdit.Text)
                    
                    GRID.TextMatrix(mRowCell, 2) = FormatNumber(mItemSel.Precio, sDecMoneda)
                    
                    If GRID.Rows - 1 = mRowCell Then
                        GRID.Rows = GRID.Rows + 1
                        GRID.Row = GRID.Rows - 1
                        GRID.Col = 0
                    End If
                    
                    TotalizarCosto
                                        
                    If mGrupoSel.Sistema And mGrupoSel.Costeable Then
                        TotalizarPrecio
                    End If
                    
                    If PuedeObtenerFoco(GRID) Then GRID.SetFocus
                    
                End If
        End Select
    End If
End Sub

Private Sub txtGrupo_Change()
    If Not mGrupoSel Is Nothing Then
        mGrupoSel.NombreGrupo = txtGrupo.Text
        TabStrip1.Tabs(TabStrip1.SelectedItem.Index).Caption = mGrupoSel.NombreGrupo
    End If
End Sub

Private Sub MostrarTotal()
    
    Dim mPrecio As Double, mPorcImp As Double
    
    If IsNumeric(txtPrecio.Text) Then
        mPrecio = CDbl(txtPrecio.Text)
    End If
    
    If cboImpuesto.ListIndex <> -1 Then
        mPorcImp = CDbl(cboImpuesto.Text)
    End If
    
    TxtTotalInformativo.Text = FormatNumber(mPrecio * (1 + (mPorcImp / 100)), sDecMoneda)
    
End Sub

Private Sub txtPrecio_Change()
    MostrarTotal
End Sub

Private Sub txtPrecio_GotFocus()
    ObtenerFoco txtPrecio
End Sub

Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 37 Then
        KeyAscii = ValidarKeyAsciiNumerico(txtPrecio, KeyAscii, True)
    End If
End Sub

Private Sub txtPrecio_LostFocus()
    If Left(txtPrecio.Text, 1) = "%" Then
        txtPrecio.Text = Mid(txtPrecio.Text, 2)
        If Not IsNumeric(txtPrecio.Text) Then txtPrecio.Text = 0
        txtPrecio.Text = CStr(Round(((1 + (CDbl(txtPrecio.Text) / 100)) * CDbl(Txtcosto.Text)), 2))
    End If
    If Not IsNumeric(txtPrecio.Text) Then txtPrecio.Text = 0
    txtPrecio.Text = Format(txtPrecio.Text, "Standard")
End Sub

Private Sub Cancelar()
    IniciarForm
    ActivarFrames False
End Sub

Private Sub BuscarPlato()
    Cancelar
    If fCls.ConectarBD Then
        If fCls.BuscarPlato() Then
            Set mGrupoSel = fCls.Plato.Grupos(1)
            ActivarFrames True
            LlenarForm True
            EstadoToolbar etbEdit
        End If
    End If
End Sub

Private Sub NuevoPlato()
    
    Dim mResul As Variant
    
    Cancelar
    fCls.NuevoPlato
    ActivarFrames True
    EstadoToolbar etbEdit
    AgregarGrupo True
    
    If sFactor = 0 Then sFactor = 1
    
    If PuedeObtenerFoco(txtProducto) Then txtProducto.SetFocus
    
End Sub

Private Sub GrabarPlato()
    
    TabStrip1.SelectedItem = TabStrip1.Tabs(1)
    
    If ValidarDatos() Then
        
        If fCls.GrabarPlato() Then
            
            'MsgBox "Configuracion Guardada", vbInformation
            Mensajes Stellar_Mensaje(156, True), False
            Cancelar
            
        End If
        
    End If
    
End Sub

Private Sub ImportarConfigPlato()
    If fCls.ConectarBD Then
        If fCls.BuscarPlato(True) Then
            TabStrip1.Tabs.Clear
            LlenarForm False
            ActivarFrames True
            EstadoToolbar etbEdit
        Else
            Cancelar
        End If
    End If
End Sub

Private Sub IniciarForm()
    
    CodTemp = Empty
    
    CargarComboImpuesto
    
    IniciarProducto
    IniciarGrupo
    
    Call EstadoToolbar(eEstadoToolBar.etbStandard)
    
    Set mGrupoSel = Nothing
    Set mItemSel = Nothing
    
    Me.TabStrip1.Tabs.Clear
    
End Sub

Private Sub IniciarProducto()
    
    Me.txtProducto.Text = Empty
    
    For i = 0 To 2
        Me.txtCategoria(i).Text = Empty
        Me.lblCategoria(i).Caption = Empty
    Next
    
    Me.Txtcosto.Text = FormatNumber(0, sDecMoneda)
    Me.txtPrecio.Text = FormatNumber(0, sDecMoneda)
    
    Me.cboImpuesto.ListIndex = 0
    
    Me.chkComentario.Value = 0
    
End Sub

Private Sub IniciarGrupo()
    IniciarCtrlGrupo
    IniciarGridGrupo
End Sub

Private Sub IniciarCtrlGrupo()
    
    Me.txtGrupo.Text = Empty
    
    With Me.cboCantidad
        .Clear
        For i = 1 To 10
            .AddItem i
            .ItemData(.NewIndex) = i
        Next i
        .ListIndex = 0
    End With
    
    Me.chkCosto.Value = 0
    Me.chkRequerido.Value = 1
    
End Sub

Private Sub IniciarGridGrupo()
    With Me.GRID
        
        .Clear
        .Cols = 3
        .Rows = 2
        .FixedCols = 0
        .FixedRows = 1
        .ScrollBars = flexScrollBarVertical
        
        EncabezadoGrid 0, Stellar_Mensaje(103), 7000    ' Producto
        EncabezadoGrid 1, Stellar_Mensaje(114), 1750    ' Cantidad
        EncabezadoGrid 2, Stellar_Mensaje(164), 2200    ' Precio Grupo
        
    End With
End Sub

Private Sub EncabezadoGrid(Col As Long, Texto As String, Ancho As Long)
    
    GRID.Row = 0
    GRID.Col = Col
    GRID.Text = Texto
    GRID.ColWidth(Col) = Ancho
    GRID.Font.Bold = True
    'grid.CellAlignment = vbAlignLeft
    
End Sub

Private Function ValidarBoton(Key As String) As Boolean
    ValidarBoton = Toolbar1.Buttons(Key).Enabled
End Function

Private Sub EstadoToolbar(Estado As eEstadoToolBar)
    With Me.Toolbar1
        .Buttons("nuevo").Enabled = Estado = etbStandard
        .Buttons("modificar").Enabled = Estado = etbStandard
        .Buttons("grabar").Enabled = Estado = etbEdit
        .Buttons("cancelar").Enabled = Estado = etbEdit
        .Buttons("config").Enabled = Estado = etbEdit
    End With
End Sub

Private Sub ActivarFrames(Activar As Boolean, Optional esBloqueo As Boolean = True)
    Me.fraGrupo.Enabled = Activar
    Me.fraPlato.Enabled = Activar
    Me.frmItems.Enabled = Activar
    Me.cmdAdd.Enabled = Activar Or esBloqueo
    Me.cmdDel.Enabled = Activar
End Sub

Private Sub LlenarForm(Optional EsBusqueda As Boolean)
    
    Dim mGrupo As clsGrupoPlato
    
    If EsBusqueda Then
        
        Me.txtProducto.Text = fCls.Plato.Producto
        Me.txtCategoria(0).Text = fCls.Plato.Departamento
        txtCategoria_LostFocus 0
        Me.txtCategoria(1).Text = fCls.Plato.Grupo
        txtCategoria_LostFocus 1
        Me.txtCategoria(2).Text = fCls.Plato.SubGrupo
        txtCategoria_LostFocus 2
        Me.txtPrecio.Text = FormatNumber(fCls.Plato.Precio, sDecMoneda)
        Me.cboImpuesto.Text = fCls.Plato.Impuesto
        Me.chkComentario.Value = IIf(fCls.Plato.UsaComentario, 1, 0)
        MinutosPreparacion = fCls.Plato.TiempoPreparacion
        
        TotalizarCosto
        
        If mGrupoSel.Sistema And mGrupoSel.Costeable Then
            TotalizarPrecio
        End If
        
    End If
    
    For Each mGrupo In fCls.Plato.Grupos
        Me.TabStrip1.Tabs.Add , , mGrupo.NombreGrupo
    Next
    
    If TabStrip1.Tabs.Count = 0 Then
        cmdAdd_Click
    End If
    
    TabStrip1.Tabs(1).Selected = True
    
End Sub

Private Sub LlenarGrupo(Index As Integer)
    
    Dim mItm As clsItemGrupo
    Dim mFila As Integer
    
    IniciarGridGrupo
    
    If fCls.Plato.Grupos.Count >= Index Then
        
        Set mGrupoSel = fCls.Plato.Grupos(Index)
        
        Me.txtGrupo.Text = mGrupoSel.NombreGrupo
        Me.chksistema.Value = IIf(mGrupoSel.Sistema, 1, 0)
        Me.chkCosto.Value = IIf(mGrupoSel.Costeable, 1, 0)
        Me.chkRequerido.Value = IIf(mGrupoSel.Requerido, 1, 0)
        Me.cboCantidad.ListIndex = mGrupoSel.Cantidad - 1
        Me.cmdDel.Enabled = Not mGrupoSel.Sistema
        
        'ActivarFrames Not mGrupoSel.Sistema, True
        
        For Each mItm In mGrupoSel.Items
            mFila = mFila + 1
            If mFila > GRID.Rows - 1 Then GRID.Rows = GRID.Rows + 1
            GRID.TextMatrix(mFila, 0) = mItm.Descripcion
            GRID.TextMatrix(mFila, 1) = mItm.Cantidad
            GRID.TextMatrix(mFila, 2) = FormatNumber(mItm.Precio, sDecMoneda)
        Next
        
    End If
    
    TotalizarCosto
    
    If mGrupoSel.Sistema And mGrupoSel.Costeable Then
        TotalizarPrecio
    End If
    
End Sub

Private Sub BuscarItem(pFila)
    
    Dim mResultado As Variant
    
    If Not mGrupoSel Is Nothing Then
        'mResultado = InterfazBusquedaProducto(fCls.Conexion, "P R O D U C T O", False)
        mResultado = BuscarInfoProducto_Basica(fCls.Conexion, , , , , VistaMarca, False)
        If Not IsEmpty(mResultado) Then
            BuscarDatosItem mResultado(0), pFila
        End If
    End If
    
End Sub

Private Sub EliminarItem(pFila)
    
    On Error Resume Next
    
    If Not mGrupoSel Is Nothing Then
        If pFila <= mGrupoSel.Items.Count Then
            Set mItemSel = mGrupoSel.Items(pFila)
            If Not mItemSel Is Nothing Then
                'If MsgBox("Esta seguro de Eliminar el Item " & mItemSel.Descripcion, vbYesNo) = vbYes Then
                If Mensajes(Replace(Stellar_Mensaje(5), "$(Item)", mItemSel.Descripcion), True) Then
                    mGrupoSel.Items.Remove pFila
                    If GRID.Rows - 1 > 1 Then
                        GRID.RemoveItem CLng(pFila)
                        TotalizarCosto
                        If mGrupoSel.Sistema And mGrupoSel.Costeable Then
                            TotalizarPrecio
                        End If
                    Else
                        IniciarGridGrupo
                    End If
                End If
            End If
        End If
    End If
    
End Sub

Private Sub BuscarDatosItem(Codigo, pFila)
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "SELECT P.c_Codigo, P.c_Descri, P.Cant_Decimales, " & _
    "P.n_CostoAct AS Costo, P.n_Precio1 AS Precio, M.n_Factor AS Factor " & _
    "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS P " & _
    "INNER JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_CODIGOS C " & _
    "ON C.c_CodNasa = P.c_Codigo " & _
    "AND C.c_Codigo = '" & Codigo & "' " & _
    "LEFT JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "ON M.c_CodMoneda = P.c_CodMoneda"
        
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQl, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        If mGrupoSel.Items.Count >= pFila Then
            Set mItemSel = mGrupoSel.Items(pFila)
            mItemSel.Cantidad = 1
            mItemSel.CodigoGrupo = mGrupoSel.CodigoGrupo
            mItemSel.CodigoPlato = mGrupoSel.CodigoPlato
            mItemSel.CodigoProducto = mRs!C_CODIGO
            mItemSel.Descripcion = mRs!C_DESCRI
            mItemSel.NumeroDec = mRs!Cant_Decimales
        Else
            Set mItemSel = mGrupoSel.Items.Add(mRs!C_CODIGO, mRs!C_DESCRI, _
            mGrupoSel.CodigoGrupo, mGrupoSel.CodigoPlato, _
            1, mRs!Cant_Decimales, CDbl((mRs!Costo * mRs!Factor) / sFactor), 0, CDbl((mRs!Precio * mRs!Factor) / sFactor))
        End If
        
        GRID.TextMatrix(pFila, 0) = mItemSel.Descripcion
        GRID.TextMatrix(pFila, 1) = mItemSel.Cantidad
        GRID.TextMatrix(pFila, 2) = FormatNumber(mItemSel.Precio, sDecMoneda)
        
        TotalizarCosto
        
        If mGrupoSel.Sistema And mGrupoSel.Costeable Then
            TotalizarPrecio
        End If
        
        GRID.Row = pFila
        GRID.Col = 1
        
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
        
    Else
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
    End If
    
End Sub

Private Function ValidarKeyAsciiNumerico(Txt As TextBox, ByRef KeyAscii As Integer, _
Optional PermiteDec As Boolean = False) As Integer
    Select Case KeyAscii
        Case 48 To 57
            ValidarKeyAsciiNumerico = KeyAscii
        Case Asc(".")
            If PermiteDec Then
                If InStr(1, Txt.Text, ".", vbTextCompare) = 0 Then
                   ValidarKeyAsciiNumerico = KeyAscii
                End If
            End If
        Case vbKeyBack
            ValidarKeyAsciiNumerico = KeyAscii
        Case Else
            KeyAscii = 0
    End Select
End Function

Private Function BuscarSqlCategoria(Index As Integer, Optional EsInterfaz As Boolean)
    Select Case Index
        Case 0
            BuscarSqlCategoria = "SELECT * FROM ( SELECT c_Codigo AS Codigo, c_Descripcio AS Descripcion " & vbNewLine & _
            "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.MA_DEPARTAMENTOS " & vbNewLine & _
            IIf(Not EsInterfaz, "WHERE c_Codigo = '" & txtCategoria(Index).Text & "' ", Empty) & ") TB WHERE 1 = 1 " & vbNewLine
        Case 1
            BuscarSqlCategoria = "SELECT * FROM ( SELECT c_Codigo AS Codigo, c_Descripcio AS Descripcion " & vbNewLine & _
            "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.MA_GRUPOS " & vbNewLine & _
            "WHERE c_Departamento = '" & txtCategoria(0).Text & "' " & vbNewLine & _
            IIf(Not EsInterfaz, "AND c_Codigo = '" & txtCategoria(Index).Text & "' ", Empty) & ") TB WHERE 1 = 1 " & vbNewLine
        Case 2
            BuscarSqlCategoria = "SELECT * FROM ( SELECT c_Codigo AS Codigo, c_Descripcio AS Descripcion " & vbNewLine & _
            "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.MA_SUBGRUPOS " & vbNewLine & _
            "WHERE c_In_Departamento = '" & txtCategoria(0).Text & "' " & GetLines & _
            "AND c_In_Grupo = '" & txtCategoria(1).Text & "' " & vbNewLine & _
            IIf(Not EsInterfaz, "AND c_Codigo = '" & txtCategoria(Index).Text & "' ", Empty) & ") TB WHERE 1 = 1 " & vbNewLine
        Case Else
            BuscarSqlCategoria = "SELECT * FROM ( " & vbNewLine & _
            "SELECT c_CodMoneda AS Codigo, c_Descripcion AS Descripcion, n_Factor as Factor " & vbNewLine & _
            "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.MA_MONEDAS " & vbNewLine & _
            IIf(Not EsInterfaz, "WHERE c_Codmoneda = '" & txtCategoria(Index).Text & "' ", Empty) & ") TB WHERE 1 = 1 " & vbNewLine
    End Select
End Function

Private Function ValidarTxtCategoria(Index As Integer) As Boolean
    
    Dim Ini As Integer
    
    Ini = Index - 1
    
    For i = Ini To 0 Step -1
        If Trim(txtCategoria(i).Text) = Empty Then
            'MsgBox "Debe elegir un " & IIf(i = 2, " Grupo ", " Departamento ")
            Mensajes Stellar_Mensaje(6) & " " & IIf(i = 1, Stellar_Mensaje(7), Stellar_Mensaje(8))
            Exit Function
        End If
    Next i
    
    ValidarTxtCategoria = True
    
End Function

Private Sub LimpiarTxtCategoria(Index As Integer)
    
    For i = Index To 2
        txtCategoria(i).Text = Empty
        lblCategoria(i).Caption = Empty
    Next i
    
    CodTemp = Empty
    
End Sub

Private Function BusquedaCategoria(Index As Integer)
    
    Dim mBusqueda As Object
    Dim mResp As Variant
    Dim mSQl As String
    
    On Error GoTo Errores
    
    If Not fCls.ConectarBD Then Exit Function
    
    Set mBusqueda = Frm_Super_Consultas
    
    mSQl = BuscarSqlCategoria(Index, True)
    
    'MsgBox mSql
    
    With mBusqueda
        
        ' D E P A R T A M E N T O S
        ' G R U P O S
        ' S U B G R U P O S
        .Inicializar mSQl, _
        IIf(Index = 0, Stellar_Mensaje(124), _
        IIf(Index = 1, Stellar_Mensaje(125), _
        IIf(Index = 2, Stellar_Mensaje(126), _
        Stellar_Mensaje(167)))), fCls.Conexion
        
        ' CODIGO
        ' DESCRIPCIÓN
        .Add_ItemLabels Stellar_Mensaje(122), "Codigo", 2500, 0
        .Add_ItemLabels Stellar_Mensaje(123), "Descripcion", 8500, 0
        
        .Add_ItemSearching Stellar_Mensaje(123), "Descripcion"
        .Add_ItemSearching Stellar_Mensaje(122), "Codigo"
        
        If Trim(Condicion) <> Empty Then
            Frm_Super_Consultas.StrCondicion = Condicion
        End If
        
        Set Frm_Super_Consultas.FormPadre = Me
        
        Frm_Super_Consultas.txtDato.Text = "%"
        
        Frm_Super_Consultas.Show vbModal
        
    End With
    
    Set mBusqueda = Nothing
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbExclamation
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Function ValidarDatos() As Boolean
    
    Dim mGrupo As clsGrupoPlato
    
    On Error GoTo Errores
    
    With fCls.Plato
        
        If txtCategoria(0).Text = Empty Then
            'MsgBox "Debe seleccionar un Departamento", vbExclamation
            Mensajes Stellar_Mensaje(9)
            Exit Function
        End If
        
        If SVal(txtPrecio.Text) = 0 Then
            'MsgBox "El precio del Producto no puede ser 0", vbExclamation
            Mensajes Stellar_Mensaje(10)
            Exit Function
        End If
        
        If fCls.Plato.Grupos.Count = 0 Then
            'MsgBox "No hay grupo configurado", vbExclamation
            Mensajes Stellar_Mensaje(11)
            Exit Function
        End If
        
        For Each mGrupo In fCls.Plato.Grupos
            If mGrupo.Items.Count = 0 And Not mGrupo.Sistema Then
                'MsgBox "El grupo " & mGrupo.NombreGrupo & " no tiene Items Asociados", vbExclamation
                Mensajes Replace(Stellar_Mensaje(101), "$(Grupo)", mGrupo.NombreGrupo)
                Exit Function
            End If
        Next
        
        With fCls.Plato
            .Producto = Me.txtProducto.Text
            .Precio = SVal(txtPrecio.Text)
            .Costo = SVal(Txtcosto.Text)
            .Departamento = txtCategoria(0).Text
            .Grupo = txtCategoria(1).Text
            .SubGrupo = txtCategoria(2).Text
            .Impuesto = SVal(cboImpuesto.List(cboImpuesto.ListIndex))
            .UsaComentario = (chkComentario.Value = vbChecked)
            .TiempoPreparacion = MinutosPreparacion
        End With
        
        ValidarDatos = True
        
    End With
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbCritical, "Validando Datos"
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Sub CargarComboImpuesto()
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    cboImpuesto.Clear
    
    mSQl = "SELECT cs_Grupo FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.MA_AUX_GRUPO WHERE cs_Tipo = 'IMP' ORDER BY cs_Grupo "
    
    If fCls.ConectarBD Then
        Set mRs = New ADODB.Recordset
        mRs.Open mSQl, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not mRs.EOF Then
            Do While Not mRs.EOF
                cboImpuesto.AddItem mRs!cs_Grupo
                mRs.MoveNext
            Loop
        End If
    End If
    
    If cboImpuesto.ListCount > 0 Then cboImpuesto.ListIndex = 0
    
End Sub

Private Sub AgregarGrupo(Optional Sistema As Boolean = False)
    fCls.Plato.Grupos.Add fCls.Plato.CodigoPlato, 0, _
    Stellar_Mensaje(157, True), False, False, 1, Sistema ' Nuevo Grupo
    'Me.txtGrupo.Text = "Nuevo Grupo"
    TabStrip1.Tabs.Add , , Stellar_Mensaje(157, True)
    TabStrip1.Tabs(TabStrip1.Tabs.Count).Selected = True
End Sub

Private Sub ObtenerFoco(TextBox As TextBox)
    TextBox.SelStart = 0
    TextBox.SelLength = Len(TextBox.Text)
End Sub

Private Sub txtProducto_Change()
    fCls.Plato.Producto = txtProducto.Text
End Sub

Private Sub txtProducto_KeyPress(KeyAscii As Integer)
    If KeyAscii = Asc("'") Then KeyAscii = 0
End Sub

Private Sub TotalizarCosto()
    
    Dim mItm As clsItemGrupo
    Dim mTotal As Double
    
    If Not mGrupoSel Is Nothing Then
        If mGrupoSel.Sistema Then
            For Each mItm In mGrupoSel.Items
                mTotal = mTotal + (mItm.Costo * mItm.Cantidad)
            Next
        End If
    End If
    
    Me.Txtcosto.Text = FormatNumber(mTotal, sDecMoneda)
    
End Sub

Private Sub TotalizarPrecio()
    
    Dim mItm As clsItemGrupo
    Dim mTotal As Double
    
    If Not mGrupoSel Is Nothing Then
        If mGrupoSel.Sistema Then
            For Each mItm In mGrupoSel.Items
                mTotal = mTotal + _
                (IIf(mItm.Precio > 0, mItm.Precio, mItm.PrecioFicha) * mItm.Cantidad)
            Next
        End If
    End If
    
    Me.txtPrecio.Text = FormatNumber(mTotal, sDecMoneda)
    
End Sub
