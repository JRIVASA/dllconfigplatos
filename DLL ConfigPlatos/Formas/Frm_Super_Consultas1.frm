VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Frm_Super_Consultas 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8970
   ClientLeft      =   1350
   ClientTop       =   1365
   ClientWidth     =   11835
   ControlBox      =   0   'False
   ForeColor       =   &H8000000F&
   Icon            =   "Frm_Super_Consultas1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8970
   ScaleMode       =   0  'User
   ScaleWidth      =   11835
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Medir 
      Height          =   135
      Left            =   9600
      ScaleHeight     =   75
      ScaleWidth      =   75
      TabIndex        =   22
      Top             =   1440
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.CommandButton CmdBuscar 
      Appearance      =   0  'Flat
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Index           =   0
      Left            =   9360
      Picture         =   "Frm_Super_Consultas1.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5910
      Width           =   1035
   End
   Begin VB.CommandButton CmdAceptarCancelar 
      Appearance      =   0  'Flat
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Index           =   0
      Left            =   10605
      Picture         =   "Frm_Super_Consultas1.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   5910
      Width           =   1035
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   12120
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   11160
         Picture         =   "Frm_Super_Consultas1.frx":9D8E
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9075
         TabIndex        =   19
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " B�squeda Avanzada "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1665
      Left            =   225
      TabIndex        =   15
      Top             =   7185
      Width           =   11475
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   " Condici�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   1245
         Left            =   7530
         TabIndex        =   12
         Top             =   180
         Width           =   2175
         Begin VB.OptionButton Opt_Operador 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "&O"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   1
            Left            =   1200
            TabIndex        =   7
            Top             =   570
            Width           =   705
         End
         Begin VB.OptionButton Opt_Operador 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "&Y"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   0
            Left            =   300
            TabIndex        =   6
            Top             =   570
            Value           =   -1  'True
            Width           =   705
         End
      End
      Begin VB.CommandButton Btn_Agregar 
         Appearance      =   0  'Flat
         Caption         =   "A&gregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1125
         Left            =   9930
         Picture         =   "Frm_Super_Consultas1.frx":BB10
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   300
         Width           =   1335
      End
      Begin MSComctlLib.ListView Lst_Avanzada 
         CausesValidation=   0   'False
         Height          =   1215
         Left            =   210
         TabIndex        =   9
         Top             =   210
         Width           =   7125
         _ExtentX        =   12568
         _ExtentY        =   2143
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   5790296
         BackColor       =   16448250
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "StrCampo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Campo"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Valor"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Operador"
            Object.Width           =   3351
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   900
      Left            =   -90
      TabIndex        =   13
      Top             =   421
      Width           =   11865
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   300
         TabIndex        =   14
         Top             =   60
         Width           =   10710
         _ExtentX        =   18891
         _ExtentY        =   1429
         ButtonWidth     =   2170
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Informaci�n"
               Key             =   "Informacion"
               Object.ToolTipText     =   "Informacion sobre el elemento Seleccionado"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir de la B�squeda"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "Ayuda del Sistema"
               ImageIndex      =   3
            EndProperty
         EndProperty
         Begin VB.Timer Tim_Progreso 
            Enabled         =   0   'False
            Interval        =   500
            Left            =   5760
            Top             =   120
         End
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1700
      Left            =   225
      TabIndex        =   11
      Top             =   5250
      Width           =   8955
      Begin VB.CheckBox Chk_Avanzada 
         Appearance      =   0  'Flat
         Caption         =   "&Avanzada"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   4
         Top             =   1155
         Width           =   1905
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3150
         TabIndex        =   0
         Top             =   495
         Width           =   4500
      End
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   495
         Width           =   2610
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   225
         Left            =   3150
         TabIndex        =   16
         Top             =   1245
         Width           =   4500
         _ExtentX        =   7938
         _ExtentY        =   397
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   1005
         Left            =   7755
         TabIndex        =   23
         Top             =   375
         Width           =   1095
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   7995
         MouseIcon       =   "Frm_Super_Consultas1.frx":D892
         MousePointer    =   99  'Custom
         Picture         =   "Frm_Super_Consultas1.frx":DB9C
         Stretch         =   -1  'True
         Top             =   600
         Width           =   600
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   2250
         X2              =   8700
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   0
         Width           =   3015
      End
      Begin VB.Label Lbl_Progreso 
         Appearance      =   0  'Flat
         Caption         =   "Progreso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   3150
         TabIndex        =   17
         Top             =   990
         Width           =   4785
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1125
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":DC48
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":F9DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":159DC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   3285
      Left            =   225
      TabIndex        =   5
      Top             =   1770
      Width           =   11350
      _ExtentX        =   20029
      _ExtentY        =   5794
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16448250
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Image CmdSelect 
      Height          =   480
      Left            =   9400
      Picture         =   "Frm_Super_Consultas1.frx":1776E
      Top             =   5280
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image CmdDown 
      Height          =   480
      Left            =   11075
      Picture         =   "Frm_Super_Consultas1.frx":1C7ED
      Top             =   5280
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image CmdUp 
      Height          =   480
      Left            =   10230
      Picture         =   "Frm_Super_Consultas1.frx":1D4B7
      Top             =   5280
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00AE5B00&
      X1              =   480
      X2              =   11575
      Y1              =   1620
      Y2              =   1620
   End
   Begin VB.Image Image1 
      Height          =   2160
      Left            =   3465
      Picture         =   "Frm_Super_Consultas1.frx":1E181
      Stretch         =   -1  'True
      Top             =   2580
      Width           =   2400
   End
   Begin VB.Label lblTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00FAFAFA&
      BackStyle       =   0  'Transparent
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   255
      Left            =   225
      TabIndex        =   10
      Top             =   1500
      Width           =   4095
   End
End
Attribute VB_Name = "Frm_Super_Consultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Public Connection As New ADODB.Connection
Public FrmPantallaInfo As Form
Public FormPadre As Form
Public StrBotonPresionado As String
Public CampoCodigo As String
Public StrCondicion As String
Public StrCadBusCod As String
Public StrCadBusDes As String
Public StrOrderBy As String

Public StrTituloForma As String
Public UsaCodigosAlternos As Boolean
Public FormatoAplicar As Boolean
Public FormatoCantStr As Long
Public FormatoRelleno As String

Public EvitarActivate As Boolean
Public BusquedaInstantanea As Boolean
Public CustomFontSize As String
Public CustomRowAdvance As String
Public ArrResultado

Private ContPuntos As Long, ContItems As Long, ContFound As Long, StrCont As String
Private isPrimeraVez As Boolean ' Esta Variable es para que cuando tenga registro y no este visible por primera vez arraque el focus en el grid
Private StrSQL As String
Private ArrCamposBusquedas()
Private ArrCamposDeLaConsulta()
Private ArrPropiedadCampo()
Private ArrRegistroSeleccionado()
Private Objlw As New obj_listview
Private Conex_SC As ADODB.Connection
Private Band As Boolean
Private StrSqlMasCondicion As String

Private Sub FrameTitulo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then MoverVentana Me.hWnd
End Sub

Private Sub lbl_Organizacion_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Sub lbl_website_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    FrameTitulo_MouseMove Button, Shift, X, Y
End Sub

Private Function IgnorarActivate() As Boolean
    EvitarActivate = True: IgnorarActivate = EvitarActivate
End Function

Private Function Consulta_Avanzada() As String
    
    Dim SqlW As String, SQL As String
    
    SQL = StrSQL
    
    If Me.Lst_Avanzada.ListItems.Count = 0 Then
        IgnorarActivate
        Mensajes Stellar_Mensaje(155) '"No ha especificado ning�n Campo de B�squeda"
        Consulta_Avanzada = ""
        
        Exit Function
    End If
    
    SqlW = " WHERE "
    
    For i = 1 To Me.Lst_Avanzada.ListItems.Count
        SqlW = SqlW + " (" + Me.Lst_Avanzada.ListItems(i) & " LIKE '" + Me.Lst_Avanzada.ListItems(i).SubItems(2) + "%') "
        If i < Me.Lst_Avanzada.ListItems.Count Then
            If Me.Lst_Avanzada.ListItems(i).SubItems(3) = "Y" Then
                SqlW = SqlW + " AND "
            Else
                SqlW = SqlW + " OR "
            End If
        End If
    Next
    
    If Trim(Me.StrCondicion) <> "" Then
        If SqlW = " WHERE " Then
            SqlW = SqlW + " (" + StrCondicion + ") "
        Else
            SqlW = SqlW + " AND (" + StrCondicion + ") "
        End If
    End If
    
    SQL = SQL + SqlW
    
    If Trim(Me.StrOrderBy) <> "" Then
        SQL = SQL + " ORDER BY " + Me.StrOrderBy
    End If
    
    Consulta_Avanzada = SQL
    
End Function

Private Sub Btn_Agregar_Click()
    
    Dim ItmX As ListItem
    
    Set ItmX = Me.Lst_Avanzada.ListItems.Add(, , ArrCamposBusquedas(Me.cmbItemBusqueda.ListIndex + 1))
    
    ItmX.ListSubItems.Add , , Me.cmbItemBusqueda.Text
    ItmX.ListSubItems.Add , , Me.txtDato
    
    If Me.Opt_Operador(0).Value = True Then
        ItmX.ListSubItems.Add , , "Y"
    Else
        ItmX.ListSubItems.Add , , "O"
    End If
    
End Sub

Private Sub cmbItemBusqueda_Click()
    Me.txtDato = ""
    
    If Band = True Then
        If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    End If
End Sub

Private Sub cmbItemBusqueda_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 49 To 57
            On Error Resume Next
            cmbItemBusqueda.ListIndex = Val(Chr(KeyAscii)) - 1
        Case vbKeyReturn
            If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    End Select
End Sub

Private Sub CmdTeclado_Click()
    
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    IgnorarActivate
    'TECLADO.Show vbModal
    TecladoWindows txtDato
    
End Sub

Private Sub CmdUp_Click()
    
    If GRID.ListItems.Count > 0 Then
        
        Dim CantSaltos As Long
        CantSaltos = CLng(Val(CustomRowAdvance))
        
        If (GRID.SelectedItem.Index - CantSaltos) <= 1 Then
            Set GRID.SelectedItem = GRID.ListItems(1)
            GRID.SelectedItem.EnsureVisible
            CmdUp.Visible = False
        Else
            Set GRID.SelectedItem = GRID.ListItems(GRID.SelectedItem.Index - CantSaltos)
            GRID.SelectedItem.EnsureVisible
        End If
        
        CmdDown.Visible = True
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
        
    End If
    
End Sub

Private Sub CmdDown_Click()
    
    If GRID.ListItems.Count > 0 Then
        
        Dim CantSaltos As Long
        CantSaltos = CLng(Val(CustomRowAdvance))
        
        If (GRID.SelectedItem.Index + CantSaltos) > GRID.ListItems.Count Then
            Set GRID.SelectedItem = GRID.ListItems(GRID.ListItems.Count)
            GRID.SelectedItem.EnsureVisible
            CmdDown.Visible = False
        Else
            Set GRID.SelectedItem = GRID.ListItems(GRID.SelectedItem.Index + CantSaltos)
            GRID.SelectedItem.EnsureVisible
        End If
        
        CmdUp.Visible = True
        If PuedeObtenerFoco(GRID) Then GRID.SetFocus
        
    End If
    
End Sub

Private Sub CmdSelect_Click()
    If GRID.ListItems.Count > 0 Then grid_DblClick
End Sub

Private Sub Exit_Click()
    txtDato_KeyDown vbKeyEscape, 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    EvitarActivate = False
    BusquedaInstantanea = False
    CustomFontSize = vbNullString
    CustomRowAdvance = vbNullString
    StrOrderBy = vbNullString
End Sub

Private Sub lblUpClickArea_Click()
    CmdUp_Click
End Sub

Private Sub lblDownClickArea_Click()
    CmdDown_Click
End Sub

Private Sub lblSelectClickArea_Click()
    CmdSelect_Click
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub Lst_Avanzada_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Me.Lst_Avanzada.ListItems.Remove Me.Lst_Avanzada.SelectedItem.Index
    End If
End Sub

Private Sub Tim_Progreso_Timer()
    ContPuntos = ContPuntos + 1
    
    If ContPuntos > 3 Then
        ContPuntos = 0
    End If
    
    Me.Lbl_Progreso.Caption = StrCont + StellarMensaje(158) + String(ContPuntos, ".") '"Buscando"
End Sub

Private Sub Chk_Avanzada_Click()
    Me.Lst_Avanzada.ListItems.Clear
    
    If Me.Chk_Avanzada.Value = 0 Then
        Me.Height = 7250
    ElseIf Me.Chk_Avanzada.Value = 1 Then
        Me.Height = 8900
    End If
End Sub

Private Sub CmdAceptarCancelar_Click(Index As Integer)
    'txtDato_KeyDown vbKeyEscape, 0
    Me.StrBotonPresionado = "Cancelar"
End Sub

Private Sub CmdBuscar_Click(Index As Integer)
    Call txtDato_KeyPress(vbKeyReturn)
End Sub

Private Sub Form_Activate()
    
    If EvitarActivate Then EvitarActivate = False: Exit Sub
    
    Dim Longitud As Long
    
    Call AjustarPantalla(Me)
    
    Medir.FontName = lblTitulo.FontName
    Medir.FontSize = lblTitulo.FontSize
    Medir.FontBold = lblTitulo.FontBold
    
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(144, True)
    
    Longitud = Medir.TextWidth(lblTitulo.Caption)
    
    If Longitud > 0 Then
        Line1.X1 = Line1.X1 + (Longitud)
    End If
    
    If Me.Chk_Avanzada.Value = 0 Then
        Me.Height = 7250
    ElseIf Me.Chk_Avanzada.Value = 1 Then
        Me.Height = 8900
    End If
    
    If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    
    If isPrimeraVez Then
        If Me.GRID.ListItems.Count Then
            If PuedeObtenerFoco(GRID) Then GRID.SetFocus
            isPrimeraVez = False
        End If
    End If
    
    Band = True
    
    txtDato.SelStart = Len(txtDato.Text)
        
    If Trim(CustomFontSize) <> vbNullString Then GRID.Font.Size = Val(CustomFontSize)
    If Trim(CustomRowAdvance) = vbNullString Then CustomRowAdvance = "7" ' Default
    If BusquedaInstantanea Then txtDato_KeyPress vbKeyReturn
    
End Sub

Private Sub Form_Load()
    
    ReDim ArrCamposBusquedas(0)
    ReDim ArrCamposDeLaConsulta(0)
    ReDim ArrRegistroSeleccionado(0)
    ReDim ArrPropiedadCampo(0)
    
    Band = True
    isPrimeraVez = True
    
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(129) 'informaci�n
    Toolbar1.Buttons(2).Caption = Stellar_Mensaje(130) 'salir
    Toolbar1.Buttons(3).Caption = Stellar_Mensaje(131) 'ayuda
    
    Label1.Caption = Stellar_Mensaje(132) 'criterios de busqueda
    Chk_Avanzada.Caption = Stellar_Mensaje(133) 'avanzada
    Lbl_Progreso.Caption = Stellar_Mensaje(134) 'progreso
    CmdBuscar(0).Caption = Stellar_Mensaje(135) 'buscar
    CmdAceptarCancelar(0).Caption = Stellar_Mensaje(136) 'cancelar
    
    Btn_Agregar.Caption = Stellar_Mensaje(137) 'agregar
    Frame1.Caption = Stellar_Mensaje(138) 'condicion
    Opt_Operador(0).Caption = Stellar_Mensaje(139) 'y
    Opt_Operador(1).Caption = Stellar_Mensaje(140) 'o
    
    Lst_Avanzada.ColumnHeaders(2).Text = Stellar_Mensaje(141) 'campo
    Lst_Avanzada.ColumnHeaders(3).Text = Stellar_Mensaje(142) 'valor
    Lst_Avanzada.ColumnHeaders(4).Text = Stellar_Mensaje(143) 'operador
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo Errores
        Select Case KeyCode
                    
            Case Is = vbKeyF1
                'Llamar Ayuda
                
            Case Is = vbKeyF2
                'Me.formPadre.Boton = 1
                'Call grid_DblClick
            
            Case Is = vbKeyF12
                txtDato_KeyDown vbKeyEscape, 0
                
        End Select
    Exit Sub
    
Errores:
    
    Unload Me
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        
        Case "Informaci�n"
            Me.FormPadre.boton = 1
            Call grid_DblClick
        
        Case "Salir"
            txtDato_KeyDown vbKeyEscape, 0
            
    End Select
End Sub

Private Sub txtDato_Click()
    If ModoTouch Then CmdTeclado_Click
End Sub

Private Sub txtDato_GotFocus()
    Set CampoT = txtDato
    txtDato.Width = 4500
    Barra_Prg.Width = 4500
    lblTeclado.Visible = True
    CmdTeclado.Visible = True
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        
        Case 27, vbKeyEscape, vbKeyF12
    
            StrBotonPresionado = "Salir"
            
            ArrResultado = Array("", "")
            
            Unload Me
            
            'Me.Hide
        
        Case vbKeyF3
            
            If PuedeObtenerFoco(cmbItemBusqueda) Then cmbItemBusqueda.SetFocus
            
        Case vbKeyF4
            
            On Error Resume Next
            
            cmbItemBusqueda.ListIndex = IIf(cmbItemBusqueda.ListIndex <> 0, 0, 1)
        
    End Select
End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    
    On Error GoTo GetError
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            If txtDato = "" Then
                If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
                Exit Sub
            End If
            
            If Me.Chk_Avanzada.Value = 1 Then
                StrSqlMasCondicion = Consulta_Avanzada
                Consulta_Mostrar
            Else
                If txtDato = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
                    StrSqlMasCondicion = StrSQL
                    
                    If Trim(Me.StrCondicion) <> "" Then
                        If InStr(StrSQL, "where") Or InStr(StrSQL, "WHERE") Then
                            StrSqlMasCondicion = StrSQL + " AND (" + Me.StrCondicion + ") "
                        Else
                            StrSqlMasCondicion = StrSQL + " WHERE (" + Me.StrCondicion + ") "
                        End If
                    End If
                    
                    Consulta_Mostrar
                Else
                    'strCadBusDes = arrCamposDeLaConsulta(Me.cmbItemBusqueda.ListIndex + 1)
                    ' Ivan Espinoza 31/08/02
                    
                    StrSqlMasCondicion = StrSQL
                    
                    'Debug.Print strSqlMasCondicion
                    
                    If Trim(Me.StrCondicion) <> "" Then
                        If InStr(UCase(StrSQL), "WHERE") Then
                            StrSqlMasCondicion = StrSQL + " AND (" + Me.StrCondicion + ") "
                        Else
                            StrSqlMasCondicion = StrSQL + " WHERE (" + Me.StrCondicion + ") "
                        End If
                    End If
                    
                    'Debug.Print strSqlMasCondicion
                    
                    StrCadBusDes = ArrCamposBusquedas(Me.cmbItemBusqueda.ListIndex + 1)
                    
                    If StrCadBusDes <> "" Then
                        'If InStr(UCase(strSQL), "WHERE") Then
                        
                        If InStr(UCase(StrSqlMasCondicion), "WHERE") Then
                            StrSqlMasCondicion = StrSqlMasCondicion + " AND " + StrCadBusDes + " LIKE '" + Me.txtDato + "%'"
                        Else
                        
                            '19695633
                            'Solo busca productos activos, los inactivos NO.
                            
                            StrSqlMasCondicion = StrSqlMasCondicion + " WHERE  " + StrCadBusDes + " LIKE '" + Me.txtDato + "%' "
                            
                        End If
                    Else
                        IgnorarActivate
                        Mensajes Stellar_Mensaje(153) '"Debe especificar Mejor la Consulta."
                        Screen.MousePointer = 0
                        
                        Exit Sub
                    End If
                    
                    'Debug.Print strSqlMasCondicion
                    'Debug.Print strSqlMasCondicion
                    
                    Consulta_Mostrar
                    
                End If
            End If
    End Select
    
    Screen.MousePointer = 0
    
    On Error GoTo 0
    
    Exit Sub
    
GetError:
    
    Call IgnorarActivate
    Mensajes Err.Description 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
End Sub

Private Sub grid_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    'a = Abs((grid.SortOrder + 1) - 2)
    'grid.SortOrder = Abs((grid.SortOrder + 1) - 2)
    
    If GRID.SortOrder = lvwAscending Then
        GRID.SortOrder = lvwDescending
    Else
        GRID.SortOrder = lvwAscending
    End If
    
    GRID.SortKey = ColumnHeader.Index - 1
    GRID.Sorted = True
End Sub

Private Sub grid_DblClick()
    
    If GRID.ListItems.Count = 0 Then Exit Sub
    
    ArrResultado = Objlw.TomardataLw(Me.GRID, Me.GRID.SelectedItem.Index)
    
    Unload Me
    
 End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Public Function Buscar(Optional ByVal EmitirMensajes As Boolean = True) As Variant
    
    Dim ArrResultados()
    Dim RsConsulta As New ADODB.Recordset
    Dim Cnx As New ADODB.Connection

    If Trim(StrSQL) = "" Then
        Exit Function
    End If
    
    If Trim(StrCondicion) <> "" Then
        If InStr(UCase(StrSQL), "WHERE") Then
            StrSQL = StrSQL + " AND (" + StrCondicion + ") "
        Else
            StrSQL = StrSQL + " WHERE (" + StrCondicion + ") "
        End If
    End If
    
    On Error GoTo GetError
    
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    
    RsConsulta.CursorLocation = adUseServer
    
    Apertura_Recordset RsConsulta
    
    RsConsulta.Open StrSQL, Cnx, adOpenKeyset, adLockReadOnly
    
    ReDim ArrResultados(RsConsulta.Fields.Count - 1)
    
    If Not RsConsulta.EOF Then
        For i = 0 To RsConsulta.Fields.Count - 1
            ArrResultados(i) = RsConsulta.Fields(i)
        Next
    Else
        For i = 0 To RsConsulta.Fields.Count - 1
            ArrResultados(i) = ""
        Next
        
        If EmitirMensajes Then
            IgnorarActivate
            Mensajes Stellar_Mensaje(154) '"No hay ning�n Item que cumpla con los Par�metros de B�squeda."
        End If
    End If
    
    RsConsulta.Close
    
    Buscar = ArrResultados
    
    Exit Function
    
GetError:
    
    If EmitirMensajes Then
        Call IgnorarActivate
        Mensajes Err.Description
    End If
    
    Err.Clear
    
End Function

Public Function Consulta_Mostrar() As Boolean
    
    Dim RsConsulta As New ADODB.Recordset
    Dim Item As ListItem
    Dim Lista As ListView
    Dim oField As ADODB.Field
    Dim Cnx As New ADODB.Connection
    Dim Cont As Long, ContItems As Long
    Dim SQL As String, Rs As New ADODB.Recordset
    Dim CodigoBusq As String
    
    Set Lista = GRID
    
    Cont = 0
    
    If Trim(StrSqlMasCondicion) = "" Then
        Exit Function
    End If
    
    Me.StrBotonPresionado = ""
    
    Call Desactivar_Objetos(False)
    
    On Error GoTo GetError
    
    Screen.MousePointer = 11
    
    GRID.ListItems.Clear
    
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    
    'CODIGO
    If Trim(UCase(Me.cmbItemBusqueda.Text)) = UCase(Stellar_Mensaje(122)) And UsaCodigosAlternos = True Then
        
        SQL = " SELECT c_codnasa AS CodMaestro, c_codigo AS CodAlterno FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_CODIGOS"
        
        'If FormatoAplicar = True Then
            'CodigoBusq = Format(Me.txtDato, String(FormatoCantStr - 1, FormatoRelleno) + "#")
        'End If
        
        CodigoBusq = Me.txtDato
        
        'strSqlMasCondicion = strSqlMasCondicion + " WHERE " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
        
        SQL = SQL + " WHERE c_Codigo LIKE '" & CodigoBusq & "%'"
        
        Rs.CursorLocation = adUseServer
        
        'Debug.Print Sql
        
        Rs.Open SQL, Cnx, adOpenKeyset, adLockReadOnly
        
        Do While Not Rs.EOF
            CodigoBusq = Rs!CodMaestro
            
            'If FormatoAplicar = True Then
                'CodigoBusq = Format(CodigoBusq, String(FormatoCantStr - 1, FormatoRelleno) + "#")
            'End If
            
            If InStr(UCase(StrSqlMasCondicion), "WHERE") Then
                StrSqlMasCondicion = StrSqlMasCondicion & " OR " & Me.CampoCodigo & " LIKE '" & CodigoBusq & "%'"
            Else
                StrSqlMasCondicion = StrSqlMasCondicion & " WHERE " & Me.CampoCodigo & " LIKE '" + CodigoBusq & "%'"
            End If
            
            Rs.MoveNext
        Loop
    End If
    
    If StrOrderBy <> "" Then
        StrSqlMasCondicion = StrSqlMasCondicion + " Order By  " + Me.StrOrderBy
        'Agregado el 10/10/2012
    Else
        If Replace(Mid(StrSqlMasCondicion, 18, 9), ",", "", 1, 9) = "C_DESCRI" Then
            'Debug.Print "Existe"
            StrSqlMasCondicion = StrSqlMasCondicion + " ORDER BY c_DESCRI"
        Else
            StrSqlMasCondicion = StrSqlMasCondicion
            'Debug.Print "No Existe"
        End If
    End If
    
    RsConsulta.CursorLocation = adUseServer
    
    Apertura_Recordset RsConsulta
    
    Debug.Print StrSqlMasCondicion
    
    'MsgBox strSqlMasCondicion
    
    Debug.Print StrSqlMasCondicion
    
    RsConsulta.Open StrSqlMasCondicion, Cnx, adOpenKeyset, adLockReadOnly
    
    If Not RsConsulta.EOF Then
        
        RsConsulta.MoveLast
        RsConsulta.MoveFirst
        
        ContFound = 0
        
        Me.Barra_Prg.Min = 0
        Me.Barra_Prg.Value = 0
        Me.Tim_Progreso.Enabled = True
        Me.Barra_Prg.Max = RsConsulta.RecordCount
        
        ContFound = RsConsulta.RecordCount
        
        Do Until RsConsulta.EOF
            
            DoEvents
            
            Set Item = Lista.ListItems.Add(, , RsConsulta.Fields(ArrCamposDeLaConsulta(1)))
            
            For a = 2 To GRID.ColumnHeaders.Count
                If Left(UCase(ArrCamposDeLaConsulta(a)), Len(ArrCamposDeLaConsulta(a)) - 1) = "N_PRECIO" Then
                    If Not ArrPropiedadCampo(a) Then
                        Item.SubItems(a - 1) = IIf(Not IsNull(RsConsulta.Fields(ArrCamposDeLaConsulta(a))), FormatNumber(RsConsulta.Fields(ArrCamposDeLaConsulta(a)), 2), "")
                    Else
                        Item.SubItems(a - 1) = IIf(Not IsNull(RsConsulta.Fields(ArrCamposDeLaConsulta(a))), Fix(RsConsulta.Fields(ArrCamposDeLaConsulta(a))), "")
                    End If
                Else
                    If Not ArrPropiedadCampo(a) Then
                        Item.SubItems(a - 1) = IIf(Not IsNull(RsConsulta.Fields(ArrCamposDeLaConsulta(a))), RsConsulta.Fields(ArrCamposDeLaConsulta(a)), "")
                    Else
                        Item.SubItems(a - 1) = IIf(Not IsNull(RsConsulta.Fields(ArrCamposDeLaConsulta(a))), Fix(RsConsulta.Fields(ArrCamposDeLaConsulta(a))), "")
                    End If
                End If
            Next a
            
            If Me.StrBotonPresionado = "Cancelar" Then
                Call Desactivar_Objetos(True)
                
                Screen.MousePointer = 0
                
                Me.Tim_Progreso.Enabled = False
                Me.Lbl_Progreso.Caption = StellarMensaje(134) '"Progreso"
                Me.Barra_Prg.Value = 0
                
                Exit Do
            End If
            
            Me.Barra_Prg.Value = ContItems
            
            RsConsulta.MoveNext
            
            ContItems = ContItems + 1
            
            StrCont = CStr(FormatNumber(ContItems, 0)) + " " & StellarMensaje(160) & " " + CStr(FormatNumber(ContFound, 0)) + " " '" de "
            
            'Me.Lbl_Progreso = "Buscando" + String(ContPuntos, ".") + String(3 - ContPuntos, " ") + "  " + StrCont
            Me.Lbl_Progreso = StellarMensaje(158) + String(ContPuntos, ".") + String(3 - ContPuntos, " ") + "  " + StrCont
            
        Loop
        
        Me.Barra_Prg.Value = Me.Barra_Prg.Max
        
        Call Desactivar_Objetos(True)
        
        If Me.Visible Then If PuedeObtenerFoco(Lista) Then Lista.SetFocus
        
    Else
        IgnorarActivate
        Mensajes Stellar_Mensaje(154) '"No hay ning�n Item que cumpla con los Par�metros de B�squeda."
        Call Desactivar_Objetos(True)
        If PuedeObtenerFoco(txtDato) Then txtDato.SetFocus
    End If
    
    RsConsulta.Close
    
    Screen.MousePointer = 0
    
    Me.Tim_Progreso.Enabled = False
    Me.Lbl_Progreso.Caption = CStr(FormatNumber(ContItems, 0)) + " " + StellarMensaje(160) & "."  '" Items Encontrados"
    Me.Barra_Prg.Value = 0
    
    On Error GoTo 0
    
    Consulta_Mostrar = True
    
    Me.StrBotonPresionado = ""
    
    Call Desactivar_Objetos(True)
        
    Exit Function
    
GetError:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    'Resume ' Debug
    
    Call Desactivar_Objetos(True)
    
    Me.StrBotonPresionado = ""
    
    Screen.MousePointer = 0
    
    Me.Tim_Progreso.Enabled = False
    Me.Lbl_Progreso.Caption = StellarMensaje(134) '"Progreso"
    Me.Barra_Prg.Value = 0
    
    Call IgnorarActivate
    Mensajes mErrorDesc & " " & "(" & mErrorNumber & ").(Consulta_Mostrar)" 'Call Mensaje(False, "Error N0." & Err.Number)
    
    Err.Clear
    
    Consulta_Mostrar = False
    
    If PuedeObtenerFoco(GRID) Then GRID.SetFocus
    
End Function

Sub Desactivar_Objetos(tipo As Boolean)
    
    Me.Frame3.Enabled = tipo
    Me.txtDato.Enabled = tipo
    
    GRID.Enabled = tipo
    GRID.Visible = tipo
    
    Me.CmdBuscar(0).Enabled = tipo
    
    CmdUp.Visible = tipo
    CmdDown.Visible = tipo
    CmdSelect.Visible = tipo
    
End Sub

Public Function Inicializar(ByVal CadenaSql As String, srtTTitulo, ConexionBD As ADODB.Connection, Optional frmPantInfo As Form) As Boolean
    
    Dim M
    
    Me.cmbItemBusqueda.Clear
    
    GRID.ListItems.Clear
    GRID.ColumnHeaders.Clear
    
    Me.Lst_Avanzada.ListItems.Clear
    Me.StrCondicion = ""
    
    intNumCol = 0
    
    ArrResultado = Empty
    
    Me.Height = 7000
    
    ReDim ArrCamposBusquedas(0)
    ReDim ArrCamposDeLaConsulta(0)
    ReDim ArrRegistroSeleccionado(0)
    ReDim ArrPropiedadCampo(0)
    
    Me.UsaCodigosAlternos = False
    
    FormatoAplicar = False
    FormatoCantStr = 0
    FormatoRelleno = ""
    
    Set Conex_SC = ConexionBD
    StrSQL = CadenaSql
    
    If Not frmPantInfo Is Nothing Then
       Set Me.FrmPantallaInfo = frmPantInfo
    End If
    
    'Me.Caption = UCase(srtTTitulo)
    
    lblTitulo = UCase(srtTTitulo)
    
End Function

Public Function Add_ItemSearching(strDescripcion, strCampoTabla)
    
    ReDim Preserve ArrCamposBusquedas(UBound(ArrCamposBusquedas) + 1)
    
    ' Para guardar los campos del registro seleccionados
    'ReDim Preserve arrRegistroSeleccionado(UBound(arrRegistroSeleccionado) + 1)
    
    'arrCamposDeLaConsulta(UBound(arrCamposDeLaConsulta)) = strCampoTabla
    
    Band = False
    
    ArrCamposBusquedas(UBound(ArrCamposBusquedas)) = strCampoTabla
    
    Me.cmbItemBusqueda.AddItem strDescripcion
    Me.cmbItemBusqueda.Text = Me.cmbItemBusqueda.List(0)
    
End Function

Public Function Add_ItemLabels(strDescripcion As String, strCampoTabla As String, intWidth As Integer, intAlignment As Integer, Optional TruncarDatos As Boolean = False)
    
    ReDim Preserve ArrCamposDeLaConsulta(UBound(ArrCamposDeLaConsulta) + 1)
    ReDim Preserve ArrPropiedadCampo(UBound(ArrPropiedadCampo) + 1)
    
    ArrPropiedadCampo(UBound(ArrPropiedadCampo)) = TruncarDatos
    ArrCamposDeLaConsulta(UBound(ArrCamposDeLaConsulta)) = strCampoTabla
    
    GRID.ColumnHeaders.Add , , UCase(strDescripcion), intWidth, intAlignment
    
End Function

Private Sub txtDato_LostFocus()
    txtDato.Width = 5500
    Barra_Prg.Width = 5500
    lblTeclado.Visible = False
    CmdTeclado.Visible = False
End Sub
