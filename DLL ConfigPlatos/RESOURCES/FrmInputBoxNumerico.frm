VERSION 5.00
Begin VB.Form FrmInputBoxNumerico 
   Appearance      =   0  'Flat
   BackColor       =   &H00333333&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6705
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   11445
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "FrmInputBoxNumerico.frx":0000
   ScaleHeight     =   6705
   ScaleWidth      =   11445
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox TxtPIN 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      HideSelection   =   0   'False
      IMEMode         =   3  'DISABLE
      Left            =   1260
      TabIndex        =   1
      Text            =   "999,999,999,999.99"
      Top             =   3240
      Width           =   4095
   End
   Begin VB.TextBox RequestScroll 
      Alignment       =   2  'Center
      BackColor       =   &H002E2C2A&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1275
      HideSelection   =   0   'False
      Left            =   840
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1680
      Width           =   5340
   End
   Begin VB.Label Exit 
      BackStyle       =   0  'Transparent
      Height          =   615
      Left            =   10560
      TabIndex        =   15
      Tag             =   "."
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Ok 
      BackStyle       =   0  'Transparent
      Height          =   975
      Left            =   7080
      TabIndex        =   14
      Tag             =   "."
      Top             =   5160
      Width           =   975
   End
   Begin VB.Label Punto 
      BackStyle       =   0  'Transparent
      Height          =   975
      Left            =   9840
      TabIndex        =   13
      Tag             =   "."
      Top             =   5160
      Width           =   975
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   975
      Index           =   0
      Left            =   8490
      TabIndex        =   12
      Tag             =   "0"
      Top             =   5085
      Width           =   975
   End
   Begin VB.Label BtDelete 
      BackStyle       =   0  'Transparent
      Height          =   615
      Left            =   5715
      TabIndex        =   11
      Top             =   3165
      Width           =   735
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   855
      Index           =   1
      Left            =   7080
      TabIndex        =   10
      Tag             =   "1"
      Top             =   1365
      Width           =   975
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   855
      Index           =   2
      Left            =   8490
      TabIndex        =   9
      Tag             =   "2"
      Top             =   1365
      Width           =   975
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   855
      Index           =   3
      Left            =   9795
      TabIndex        =   8
      Tag             =   "3"
      Top             =   1365
      Width           =   1095
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   975
      Index           =   4
      Left            =   7080
      TabIndex        =   7
      Tag             =   "4"
      Top             =   2565
      Width           =   1095
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   975
      Index           =   5
      Left            =   8490
      TabIndex        =   6
      Tag             =   "5"
      Top             =   2565
      Width           =   975
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   975
      Index           =   6
      Left            =   9795
      TabIndex        =   5
      Tag             =   "6"
      Top             =   2565
      Width           =   1095
   End
   Begin VB.Label Bt 
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Height          =   855
      Index           =   7
      Left            =   7080
      TabIndex        =   4
      Tag             =   "7"
      Top             =   3885
      Width           =   975
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   855
      Index           =   8
      Left            =   8490
      TabIndex        =   3
      Tag             =   "8"
      Top             =   3885
      Width           =   975
   End
   Begin VB.Label Bt 
      BackStyle       =   0  'Transparent
      Height          =   855
      Index           =   9
      Left            =   9795
      TabIndex        =   2
      Tag             =   "9"
      Top             =   3885
      Width           =   975
   End
End
Attribute VB_Name = "FrmInputBoxNumerico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public PosActText As Integer
Public Numero As Double
Public NumeroCaption As String
Public SoloEntero As Boolean

Public Sub InsertarEnPosicion(ByRef ObjTextBox As Object, XPosicion As Integer, Caracter As String)
    If EscribirMayuscula Then
        Caracter = UCase(Caracter)
    Else
        Caracter = LCase(Caracter)
    End If
    If Len(ObjTextBox) = XPosicion Then
        ObjTextBox = ObjTextBox & Caracter
    Else
        If EventoTeclado.GetKeyState(vbKeyInsert) = 1 Then
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion) & Caracter & Mid(ObjTextBox, XPosicion + 1, Len(ObjTextBox))
        Else
            ObjTextBox = Mid(ObjTextBox, 1, XPosicion) & Caracter & Mid(ObjTextBox, XPosicion + 2, Len(ObjTextBox))
        End If
    End If
    Call MoverAPosicion(ObjTextBox, XPosicion + 1, 0)
End Sub

Public Sub BorrarCaracterIzquierda(ByRef ObjTextBox As Object, XPosicion As Integer)
    If Len(ObjTextBox) > 0 And XPosicion > 0 Then
        ObjTextBox = Mid(ObjTextBox, 1, XPosicion - 1) & Mid(ObjTextBox, XPosicion + 1, Len(ObjTextBox))
        Call MoverAPosicion(ObjTextBox, XPosicion - 1, 0)
    End If
End Sub

Public Sub MoverAPosicion(ByRef ObjTextBox As Object, XPosicion As Integer, Optional Longitud As Integer = 0)
    If XPosicion < 0 Then XPosicion = 0
    If XPosicion > Len(ObjTextBox) Then XPosicion = Len(ObjTextBox)
    ObjTextBox.SelStart = XPosicion
    ObjTextBox.SelLength = Longitud
    ObjTextBox.SetFocus
    PosActText = XPosicion
End Sub

Private Sub BtDelete_Click()
    'PosActText = TxtPIN.SelStart
    'Call BorrarCaracterIzquierda(TxtPIN, PosActText)
    'Call Me.MoverAPosicion(TxtPIN, PosActText)
    SendKeys vbBack
End Sub

Private Sub Bt_Click(Index As Integer)

    PosActText = TxtPIN.SelStart
    
    Select Case Index
        Case 1
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 2
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 3
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 4
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 5
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 6
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 7
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 8
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 9
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
        Case 0
            'Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Bt(Index).Tag))
            'Call Me.MoverAPosicion(TxtPIN, PosActText)
            SendKeys (Bt(Index).Tag)
    End Select
End Sub

Private Sub Exit_Click()
    Me.Numero = -1
    Me.NumeroCaption = Empty
    Unload Me
End Sub

Private Sub Form_Activate()
    TxtPIN.SetFocus
    Me.RequestScroll.Enabled = False
End Sub

Private Sub Form_Load()
    TxtPIN.Text = Empty
    If Me.Numero > 0 Then
        TxtPIN.Text = Numero
        'TxtPIN.SelStart = Len(TxtPIN)
        SeleccionarTexto TxtPIN
    End If
End Sub

Private Sub Ok_Click()
    If TxtPIN.Text <> "" Then
        Me.NumeroCaption = TxtPIN.Text
        Me.Numero = SVal(Me.NumeroCaption)
        Unload Me
    End If
End Sub

Private Sub Punto_Click()
    If Not SoloEntero Then
        PosActText = TxtPIN.SelStart
        Call InsertarEnPosicion(TxtPIN, PosActText, UCase(Punto.Tag))
        Call Me.MoverAPosicion(TxtPIN, PosActText)
    End If
    'SendKeys vbKeyDecimal
End Sub

Private Sub TxtPIN_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 48 To 57
        Case Asc("."), vbKeyDelete
            KeyAscii = IIf(InStr(1, TxtPIN.Text, ".", vbTextCompare) > 0, 0, KeyAscii)
        Case 8 ' vbBack
        Case vbKeyReturn
            If TxtPIN.Text <> "" Then
                Ok_Click
            End If
            
        Case vbKeyF12, vbKeyEscape
            Exit_Click
        Case Else
            KeyAscii = 0
    End Select
End Sub
