VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGrupoPlato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigoPlato                                                     As Long
Private mvarCodigoGrupo                                                     As Long
Private mvarNombreGrupo                                                     As String
Private mvarRequerido                                                       As Boolean
Private mvarCosteable                                                       As Boolean
Private mvarSistema                                                         As Boolean
Private mvarCantidad                                                        As Double
Private mItems                                                              As clsItems

Property Get Items() As clsItems
    Set Items = mItems
End Property

Property Let Cantidad(ByVal Valor As Double)
    mvarCantidad = Valor
End Property

Property Get Cantidad() As Double
    Cantidad = mvarCantidad
End Property

Property Let Costeable(ByVal Valor As Boolean)
    mvarCosteable = Valor
End Property

Property Get Costeable() As Boolean
    Costeable = mvarCosteable
End Property

Property Let Requerido(ByVal Valor As Boolean)
    mvarRequerido = Valor
End Property

Property Get Requerido() As Boolean
    Requerido = mvarRequerido
End Property

Property Let NombreGrupo(ByVal Valor As String)
    mvarNombreGrupo = Valor
End Property

Property Get NombreGrupo() As String
    NombreGrupo = mvarNombreGrupo
End Property

Property Let CodigoGrupo(ByVal Valor As Long)
    mvarCodigoGrupo = Valor
End Property

Property Get CodigoGrupo() As Long
    CodigoGrupo = mvarCodigoGrupo
End Property

Property Let CodigoPlato(ByVal Valor As Long)
    mvarCodigoPlato = Valor
End Property

Property Get CodigoPlato() As Long
    CodigoPlato = mvarCodigoPlato
End Property

Property Let Sistema(ByVal Valor As Boolean)
    mvarSistema = Valor
End Property

Property Get Sistema() As Boolean
    Sistema = mvarSistema
End Property

Private Sub Class_Initialize()
    Set mItems = New clsItems
End Sub

Private Sub Class_Terminate()
    Set mItems = Nothing
End Sub
