VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConfigPlatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Enum eTipoInterfaz
    etiConfiguracion
    etiPos
End Enum

Enum eTipoProducto
    etpUnidad
    etpPesado
    etpPesable
    etpCaracExt
    etpInformativo
    etpCompuesto
End Enum

Private mConexion                                               As ADODB.Connection
Private cPlato                                                  As clsPlato
Private cGrupo                                                  As clsGrupoPlato
Private cItem                                                   As clsItemGrupo
Private mServidor                                               As String
Private mBaseDatos                                              As String
Private mUsuario                                                As String
Private mPassword                                               As String
Private mProveedor                                              As String
Private mCmdTO                                                  As Integer
Private mCnnTO                                                  As Integer
Private mTipoInterfaz                                           As eTipoInterfaz

Const TblPlatos                                                 As String = "MA_PLATOS"
Const TblGrupos                                                 As String = "MA_PLATOS_GRUPOS"
Const TblGruposPro                                              As String = "MA_PLATOS_GRUPOS_PRO"

Property Get Conexion() As ADODB.Connection
    Set Conexion = mConexion
End Property

Property Get Plato() As clsPlato
    Set Plato = cPlato
End Property

Property Let Entrada(ByVal pValor As Object)
    Set FrmAppLink = pValor
End Property

Public Sub IniciarInterfaz(Srv As String, TipoInterfaz As eTipoInterfaz, _
Optional BD As String = "VAD20", _
Optional User As String = "SA", _
Optional Pwd As String = "", _
Optional CmdTo As Integer = 30, _
Optional CnnTo As Integer = 10, _
Optional ProveedorDatos As String = "SQLOLEDB.1")
    
    If FrmAppLink Is Nothing Then
        Mensajes "El m�dulo no esta correctamente instalado / registrado." & _
        vbNewLine & "Por favor notifique al personal de Soporte T�cnico."
        Exit Sub
    End If
    
    Dim TmpVal As Variant
    TmpVal = GetEnvironmentVariable("ProgramW6432")
    
    Select Case Len(Trim(TmpVal))
        Case 0 ' Default / No Especificado.
            WindowsArchitecture = [32Bits]
        Case Else
            WindowsArchitecture = [64Bits]
    End Select
    
    ' -----------------------------
    
    mServidor = Srv
    mBaseDatos = FrmAppLink.Srv_Remote_BD_POS 'BD
    mUsuario = User
    mPassword = Pwd
    
    mCmdTO = CmdTo
    mCnnTO = CnnTo
    
    mProveedor = ProveedorDatos
    mTipoInterfaz = TipoInterfaz
    
    Select Case mTipoInterfaz
        Case etiConfiguracion
            Set frmConfigPlatos.fCls = Me
            frmConfigPlatos.Show vbModal
            Set frmConfigPlatos = Nothing
        Case etiPos
        Case Else
    End Select
    
End Sub

Public Function ConectarBD() As Boolean
    
    On Error GoTo Errores
    
    Set mConexion = New ADODB.Connection
    
    mConexion.ConnectionTimeout = mCnnTO
    
    mConexion.Open "Provider=" & mProveedor & ";Initial Catalog=" & FrmAppLink.Srv_Remote_BD_POS & ";Data Source=" & mServidor & ";" _
    & IIf(mUsuario = vbNullString Or mPassword = vbNullString, _
    "Persist Security Info=False;User ID=" & mUsuario & ";", _
    "Persist Security Info=True;User ID=" & mUsuario & ";Password=" & mPassword & ";")
    
    ConectarBD = True
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbCritical, "Conectando " & mServidor
    Mensajes Err.Description
    Err.Clear
    
End Function

Public Sub NuevoPlato()
    Set cPlato = New clsPlato
End Sub

'Public Function BusquedaProducto() As Boolean
'
'    Dim mResultado As Variant
'
'    On Error GoTo Errores
'
'    mResultado = InterfazBusquedaProducto(mConexion, Stellar_Mensaje(127), True) ' P R O D U C T O S
'
'    If Not IsEmpty(mResultado) Then
'        If Not PoseeConfiguracion(CStr(mResultado(0))) Then
'            BusquedaProducto = BuscarDatosPlato(0, CStr(mResultado(0)), CStr(mResultado(1)))
'        End If
'    End If
'
'    Set mBusqueda = Nothing
'
'    Exit Function
'
'Errores:
'
'    'MsgBox Err.Description, vbCritical, "Busqueda Producto "
'    Mensajes Err.Description
'    Err.Clear
'
'End Function

Public Function BuscarPlato(Optional esConfig As Boolean = False) As Boolean
    
    Dim mBusqueda As Object
    Dim mResultado As Variant
    Dim mSQl As String
    
    On Error GoTo Errores
    
    Set mBusqueda = Frm_Super_Consultas
    
    mSQl = "SELECT DISTINCT c_Codigo, c_Descripcion, n_Codigo, b_UsaComentarios, " & _
    "n_MinutosPreparacion, c_InfoPreparacion, c_EnlaceArchivoAdicional " & _
    "FROM " & TblPlatos
    
    With mBusqueda
        
        .Inicializar mSQl, Stellar_Mensaje(121), Conexion ' M O D I F I E R S
        
        .Add_ItemLabels Stellar_Mensaje(122), "c_Codigo", 2760, 0 ' CODIGO
        .Add_ItemLabels Stellar_Mensaje(123), "c_Descripcion", 8340, 0 ' DESCRIPCION
        .Add_ItemLabels "", "n_Codigo", 0, 0
        .Add_ItemLabels "", "b_UsaComentarios", 0, 0
        .Add_ItemLabels "", "n_MinutosPreparacion", 0, 0
        .Add_ItemLabels "", "c_InfoPreparacion", 0, 0
        .Add_ItemLabels "", "c_EnlaceArchivoAdicional", 0, 0
        
        .Add_ItemSearching Stellar_Mensaje(123), "c_Descripcion"
        .Add_ItemSearching Stellar_Mensaje(122), "c_Codigo"
        
        If Trim(Condicion) <> Empty Then
            Frm_Super_Consultas.StrCondicion = Condicion
        End If
        
        Frm_Super_Consultas.txtDato.Text = "%"
        
        Frm_Super_Consultas.Show vbModal
        
        If Not IsEmpty(Frm_Super_Consultas.ArrResultado) Then
            If Trim(Frm_Super_Consultas.ArrResultado(0)) <> Empty Then
                If Not esConfig Then
                    CodTemp = Empty
                    BuscarPlato = BuscarDatosPlato(CLng(Trim(Frm_Super_Consultas.ArrResultado(2))), _
                    CStr(Trim(Frm_Super_Consultas.ArrResultado(0))), _
                    CStr(Trim(Frm_Super_Consultas.ArrResultado(1))), _
                    CBool(Frm_Super_Consultas.ArrResultado(3)), _
                    CDbl(Frm_Super_Consultas.ArrResultado(4)), _
                    CStr(Trim(Frm_Super_Consultas.ArrResultado(5))), _
                    CStr(Trim(Frm_Super_Consultas.ArrResultado(6))))
                Else
                    BuscarPlato = BuscarConfiguracionPlato( _
                    CLng(Trim(Frm_Super_Consultas.ArrResultado(2))), True)
                End If
            End If
        End If
        
    End With
    
    Set mBusqueda = Nothing
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbCritical, "Busqueda Producto "
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Function PoseeConfiguracion(Codigo As String) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    On Error Resume Next
    
    mSQl = "SELECT c_Codigo FROM " & TblPlatos & " WHERE c_Codigo = '" & Codigo & "'"
    
    Set mRs = New ADODB.Recordset
    
    If Not ConectarBD() Then Exit Function
    
    mRs.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        'MsgBox "El producto ya posee una configuracion, Elija modificar si quiere cambiarla", vbExclamation
        Mensajes Stellar_Mensaje(102)
        PoseeConfiguracion = True
        Exit Function
    End If
    
    If Err.Description <> Empty Then
        'MsgBox Err.Description, vbCritical, "Consultado Configuracion"
        Mensajes Err.Description
        PoseeConfiguracion = True
    End If
    
    Err.Clear
    
End Function

Private Function BuscarDatosPlato(CodigoPlato As Long, CodigoPro As String, Descripcion As String, _
UsaComentario As Boolean, pTiempoPreparacion As Double, pInfoPreparacion As String, _
pEnlacesAdicionales As String) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    Set cPlato = New clsPlato
    cPlato.CodigoPlato = CodigoPlato
    cPlato.CodigoProducto = CodigoPro
    cPlato.Producto = Descripcion
    cPlato.UsaComentario = UsaComentario
    cPlato.TiempoPreparacion = pTiempoPreparacion
    cPlato.InfoPreparacion = pInfoPreparacion
    cPlato.EnlacesAdicionales = pEnlacesAdicionales
    
    mSQl = "SELECT MA_PRODUCTOS.n_Precio1 AS Precio, MA_PRODUCTOS.n_Impuesto1 AS Imp, " & _
    "MA_PRODUCTOS.c_Departamento AS DPTO, MA_PRODUCTOS.c_Grupo AS GRU, " & _
    "MA_PRODUCTOS.c_Subgrupo AS SUB, MA_PRODUCTOS.c_CodMoneda AS CodMon, " & _
    "MA_MONEDAS.c_Descripcion AS DesMon, MA_MONEDAS.n_Factor AS MonFac, " & _
    "MA_MONEDAS.n_Decimales AS MonDec " & _
    "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS " & _
    "INNER JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS " & _
    "ON MA_MONEDAS.c_CodMoneda = MA_PRODUCTOS.c_CodMoneda " & _
    "WHERE c_Codigo = '" & CodigoPro & "' "
    
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        cPlato.Precio = mRs!Precio
        cPlato.Departamento = mRs!DPTO
        cPlato.Grupo = mRs!GRU
        cPlato.SubGrupo = mRs!Sub
        cPlato.Impuesto = mRs!IMP
        
        frmConfigPlatos.txtCategoria(3).Text = mRs!CodMon
        frmConfigPlatos.lblCategoria(3).Caption = mRs!DesMon
        
        sFactor = CDec(mRs!MonFac)
        sDecMoneda = mRs!MonDec
        
    End If
    
    If cPlato.CodigoPlato > 0 Then
        BuscarDatosPlato = BuscarConfiguracionPlato(CodigoPlato)
    Else
        BuscarDatosPlato = BuscarPartesProducto
    End If
    
End Function

Private Function BuscarPartesProducto() As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    Dim mGrupo As clsGrupoPlato
    
    On Error GoTo Errores
    
    mSQl = "SELECT P.c_Codigo, P.c_Descri, P.Cant_Decimales, PA.n_Cantidad " & _
    "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS P " & _
    "INNER JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PARTES PA " & _
    "ON PA.c_Parte = P.c_Codigo " & _
    "AND PA.c_Codigo = '" & cPlato.CodigoProducto & "' "
    
    Set mRs = New ADODB.Recordset
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        Set mGrupo = cPlato.Grupos.Add(0, 0, "Producto", True, False, mRs.RecordCount, True)
        
        Do While Not mRs.EOF
            mGrupo.Items.Add mRs!C_CODIGO, mRs!C_DESCRI, 0, 0, mRs!n_Cantidad, mRs!Cant_Decimales, 0, 0, 0
            mRs.MoveNext
        Loop
        
    End If
    
    BuscarPartesProducto = True
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbCritical, "Buscando Partes Producto"
    Mensajes Err.Description
    
End Function

Private Function BuscarConfiguracionPlato(CodigoPlato As Long, Optional esConfig As Boolean = False)
    
    Dim mRs As ADODB.Recordset
    Dim mRsItm As ADODB.Recordset
    Dim mSQl As String
    Dim mGrupo As clsGrupoPlato
    
    On Error GoTo Errores
    
    mSQl = "SELECT n_Codigo, n_Plato, c_Descripcion, n_Cantidad, b_Requerido, b_Costeable, b_Sistema " & _
    "FROM " & TblGrupos & " " & _
    "WHERE n_Plato = " & CodigoPlato & " " & _
    IIf(esConfig, "AND b_Sistema = 0 ", Empty) & _
    "ORDER BY n_Codigo "
    
    Set mRs = New ADODB.Recordset
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If esConfig Then
        EliminarGrupos
    End If
    
    Do While Not mRs.EOF
        
        Set mGrupo = cPlato.Grupos.Add(mRs!n_Plato, mRs!n_Codigo, mRs!c_Descripcion, _
        mRs!b_Requerido, mRs!b_Costeable, mRs!n_Cantidad, mRs!b_Sistema)
        
        Set mRsItm = New ADODB.Recordset
        
        mSQl = "SELECT n_Grupo, n_Plato, c_Producto, P.c_Descri, n_Cantidad, n_PrecioGrupo, " & _
        "G.n_Decimales, P.n_CostoAct * (M.n_Factor / " & CDec(sFactor) & ") AS Costo, " & _
        "P.n_Precio1 * (M.n_Factor / " & CDec(sFactor) & ") AS Precio " & _
        "FROM " & TblGruposPro & " G " & _
        "INNER JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS P " & _
        "ON p.c_Codigo = c_Producto " & _
        "INNER JOIN " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
        "ON M.c_CodMoneda = p.c_CodMoneda " & _
        "WHERE n_Grupo = " & mGrupo.CodigoGrupo & " " & _
        "AND n_Plato = " & mGrupo.CodigoPlato & " " & _
        "ORDER BY n_Linea "
        
        mRsItm.Open mSQl, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        Do While Not mRsItm.EOF
            
            mGrupo.Items.Add mRsItm!c_Producto, mRsItm!C_DESCRI, _
            mGrupo.CodigoGrupo, cPlato.CodigoPlato, mRsItm!n_Cantidad, _
            mRsItm!n_Decimales, mRsItm!Costo, mRsItm!n_PrecioGrupo, mRsItm!Precio
            
            mRsItm.MoveNext
            
        Loop
        
        mRs.MoveNext
        
    Loop
    
    BuscarConfiguracionPlato = True
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbCritical, "Buscando Configuracion Plato"
    Mensajes Err.Description
    
End Function

Public Function GrabarPlato() As Boolean
    
    Dim mCorrelativoPro As String
    Dim mCorrelativo As Long
    Dim TrPendPlatos As Boolean
    
    On Error GoTo Errores
    
    mConexion.BeginTrans
    
    TrPendPlatos = ExisteTablaV3("TR_PEND_PLATOS", mConexion, "" & FrmAppLink.Srv_Remote_BD_ADM & "")
    
    mCorrelativoPro = cPlato.CodigoProducto
    
    If cPlato.CodigoProducto = Empty Then
        cPlato.CodigoProducto = BuscarCorrelativoProducto(mConexion, "Cod_Producto")
    End If
    
    If Not GrabarProducto(mConexion, cPlato.CodigoProducto) Then
        Exit Function
    End If
    
    If cPlato.CodigoPlato = 0 Then
        
        mCorrelativo = BuscarCorrelativo(mConexion, "PlatoFOOD")
        
    Else
        
        If mAplicaTRPend And TrPendPlatos Then
            
            mConexion.Execute "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PLATOS " & _
            "(n_Codigo, c_Codigo, c_Descripcion, b_UsaComentarios, n_MinutosPreparacion, c_InfoPreparacion, c_EnlaceArchivoAdicional, TipoCambio) " & vbNewLine & _
            "SELECT n_Codigo, c_Codigo, c_Descripcion, b_UsaComentarios, n_MinutosPreparacion, c_InfoPreparacion, c_EnlaceArchivoAdicional, 1 AS TipoCambio " & _
            "FROM " & TblPlatos & " WHERE n_Codigo = " & cPlato.CodigoPlato & " ", TmpRecAffec
            
            mConexion.Execute "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PLATOS_GRUPOS " & _
            "(n_Codigo, n_Plato, c_Descripcion, n_Cantidad, b_Requerido, b_Costeable, b_Sistema, TipoCambio) " & vbNewLine & _
            "SELECT n_Codigo, n_Plato, c_Descripcion, n_Cantidad, b_Requerido, b_Costeable, b_Sistema, 1 AS TipoCambio " & _
            "FROM " & TblGrupos & " WHERE n_Plato = " & cPlato.CodigoPlato & " ", TmpRecAffec
            
            mConexion.Execute "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PLATOS_GRUPOS_PRO " & _
            "(n_Grupo, n_Plato, c_Producto, n_Cantidad, n_Decimales, n_Linea, n_PrecioGrupo, TipoCambio) " & vbNewLine & _
            "SELECT n_Grupo, n_Plato, c_Producto, n_Cantidad, n_Decimales, n_Linea, n_PrecioGrupo, 1 AS TipoCambio " & _
            "FROM " & TblGruposPro & " WHERE n_Plato = " & cPlato.CodigoPlato & " ", TmpRecAffec
            
        End If
        
        mConexion.Execute "DELETE FROM " & TblPlatos & " WHERE n_Codigo = " & cPlato.CodigoPlato & " "
        mConexion.Execute "DELETE FROM " & TblGrupos & " WHERE n_Plato = " & cPlato.CodigoPlato & " "
        mConexion.Execute "DELETE FROM " & TblGruposPro & " WHERE n_Plato = " & cPlato.CodigoPlato & " "
        
        mCorrelativo = cPlato.CodigoPlato
        
    End If
    
    mConexion.Execute _
    "INSERT INTO " & TblPlatos & "(n_Codigo, c_Codigo, c_Descripcion, " & _
    "b_UsaComentarios, n_MinutosPreparacion, c_InfoPreparacion, c_EnlaceArchivoAdicional) " & _
    "VALUES (" & _
    "" & mCorrelativo & ", '" & cPlato.CodigoProducto & "', '" & Left(cPlato.Producto, 50) & "', " & _
    IIf(cPlato.UsaComentario, 1, 0) & ", " & cPlato.TiempoPreparacion & ", '', '') "
    
    If mAplicaTRPend And TrPendPlatos Then
        
        mConexion.Execute _
        "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PLATOS " & _
        "(n_Codigo, c_Codigo, c_Descripcion, b_UsaComentarios, n_MinutosPreparacion, " & _
        "c_InfoPreparacion, c_EnlaceArchivoAdicional, TipoCambio) " & _
        "VALUES (" & _
        "" & mCorrelativo & ", '" & cPlato.CodigoProducto & "', '" & Left(cPlato.Producto, 50) & "', " & _
        IIf(cPlato.UsaComentario, 1, 0) & ", " & cPlato.TiempoPreparacion & ", '', '', 0) "
        
    End If
    
    i = 0
    
    For Each cGrupo In cPlato.Grupos
        
        i = i + 1
        n = 0
        
        mConexion.Execute _
        "INSERT INTO " & TblGrupos & " " & _
        "(n_Codigo, n_Plato, c_Descripcion, n_Cantidad, b_Requerido, b_Costeable, b_Sistema) " & _
        "VALUES " & _
        "(" & i & ", " & mCorrelativo & ", '" & Left(cGrupo.NombreGrupo, 50) & "', " & _
        cGrupo.Cantidad & ", " & IIf(cGrupo.Requerido, 1, 0) & ", " & _
        IIf(cGrupo.Costeable, 1, 0) & ", " & IIf(cGrupo.Sistema, 1, 0) & ") "
        
        If mAplicaTRPend And TrPendPlatos Then
            
            mConexion.Execute _
            "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PLATOS_GRUPOS " & _
            "(n_Codigo, n_Plato, c_Descripcion, n_Cantidad, b_Requerido, b_Costeable, b_Sistema, TipoCambio) " & _
            "VALUES " & _
            "(" & i & ", " & mCorrelativo & ", '" & Left(cGrupo.NombreGrupo, 50) & "', " & _
            cGrupo.Cantidad & ", " & IIf(cGrupo.Requerido, 1, 0) & ", " & _
            IIf(cGrupo.Costeable, 1, 0) & ", " & IIf(cGrupo.Sistema, 1, 0) & ", 0) "
            
        End If
        
        For Each cItem In cGrupo.Items
            
            n = n + 1
            
            mConexion.Execute _
            "INSERT INTO " & TblGruposPro & " " & _
            "(n_Grupo, n_Plato, c_Producto, n_Cantidad, n_PrecioGrupo, n_Decimales, n_Linea) " & _
            "VALUES " & _
            "(" & i & ", " & mCorrelativo & ", '" & cItem.CodigoProducto & "', " & _
            cItem.Cantidad & ", " & cItem.Precio & ", " & cItem.NumeroDec & ", " & n & ") "
            
            If mAplicaTRPend And TrPendPlatos Then
                
                mConexion.Execute _
                "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PLATOS_GRUPOS_PRO " & _
                "(n_Grupo, n_Plato, c_Producto, n_Cantidad, n_PrecioGrupo, n_Decimales, n_Linea, TipoCambio) " & _
                "VALUES " & _
                "(" & i & ", " & mCorrelativo & ", '" & cItem.CodigoProducto & "', " & _
                cItem.Cantidad & ", " & cItem.Precio & ", " & cItem.NumeroDec & ", " & n & ", 0) "
                
            End If
            
        Next
        
    Next
    
    mConexion.CommitTrans
    
    GrabarPlato = True
    
    Exit Function
    
Errores:
    
    mConexion.RollbackTrans
    cPlato.CodigoProducto = mCorrelativoPro
    'MsgBox Err.Description, vbCritical, "Grabando Configuracion"
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Function BuscarCorrelativoProducto(Conexion As ADODB.Connection, Campo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    On Error GoTo Errores
    
    Set mRs = New ADODB.Recordset
    Dim caracter_codigo As Long
    
    'caracter_codigo = CLng(Conexion.Execute("select caracter_codigo from " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.reglas_comerciales"))
    mSQl = "select caracter_codigo from " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.reglas_comerciales"

    mRs.Open mSQl, Conexion, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        caracter_codigo = mRs!caracter_codigo
    End If
    
    mRs.Close
    
    mSQl = "Select * from " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.ma_correlativos where cu_campo = '" & Campo & "' "
    mRs.Open mSQl, Conexion, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_valor = mRs!nu_valor + 1
        mRs.Update
        'BuscarCorrelativo = Format(mRs!nu_valor, mRs!cu_formato)
        BuscarCorrelativoProducto = Format(mRs!nu_valor, String(caracter_codigo, "0"))
    Else
        mRs.AddNew
        mRs!cu_campo = Campo
        mRs!cu_descripcion = vbNullString
        mRs!nu_valor = 1
        mRs!cu_formato = "0"
        mRs.Update
        BuscarCorrelativoProducto = Format(mRs!nu_valor, String(caracter_codigo, "0"))
    End If
    
    Exit Function
    
Errores:
    
    Conexion.RollbackTrans
    'MsgBox Err.Description, vbCritical, "Correlativos"
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Function BuscarCorrelativo(Conexion As ADODB.Connection, Campo As String, _
Optional FormatoStr As Boolean = True) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    On Error GoTo Errores
    
    mSQl = "Select * from " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.ma_correlativos where cu_campo = '" & Campo & "' "
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQl, Conexion, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_valor = mRs!nu_valor + 1
        mRs.Update
        If FormatoStr Then
            BuscarCorrelativo = Format(mRs!nu_valor, mRs!cu_formato)
        Else
            BuscarCorrelativo = mRs!nu_valor
        End If
    Else
        mRs.AddNew
            mRs!cu_campo = Campo
            mRs!cu_descripcion = vbNullString
            mRs!nu_valor = 1
            mRs!cu_formato = "0"
        mRs.Update
        If FormatoStr Then
            BuscarCorrelativo = Format(1, mRs!cu_formato)
        Else
            BuscarCorrelativo = 1
        End If
    End If
    
    Exit Function
    
Errores:
    
    Conexion.RollbackTrans
    'MsgBox Err.Description, vbCritical, "Correlativos"
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Sub EliminarGrupos()
    
    Dim mG As clsGrupoPlato
    Dim i As Integer
    
    For Each mG In cPlato.Grupos
        i = i + 1
        If Not mG.Sistema Then
            cPlato.Grupos.Remove i
            i = i - 1
        End If
    Next
    
End Sub

'************************************** GRABAR PRODUCTO STELLAR *****************************************

Private Function GrabarProducto(pCn As ADODB.Connection, pCodigo As String) As Boolean
    
    Dim mProductoNuevo As Boolean
    
    On Error GoTo Errores
    
    GrabarDatosProducto pCn, pCodigo, mProductoNuevo, False
    
    If mProductoNuevo Then
        AgregarCodigo pCn, pCodigo
        AgregarCodigo pCn, pCodigo, True
    End If
    
    GrabarPartes pCn, pCodigo
    GrabarProducto = True
    
    Exit Function
    
Errores:
    
    pCn.RollbackTrans
    'MsgBox Err.Description, vbCritical
    Mensajes Err.Description
    Err.Clear
    
End Function

Private Sub GrabarDatosProducto(pCn As ADODB.Connection, pCodigo As String, _
ByRef ProductoNuevo As Boolean, TblPend As Boolean)
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    Dim mMoneda As String
    
    If Not TblPend Then
        mSQl = "SELECT * FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PRODUCTOS " & _
        "WHERE c_Codigo = '" & pCodigo & "' "
    End If
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQl, pCn, adOpenKeyset, adLockOptimistic, adCmdText
    
    If mRs.EOF Then
        ProductoNuevo = True
        mRs.AddNew
        mRs!f_inicial = DateSerial(1980, 1, 1)
        mRs!f_final = DateSerial(1980, 1, 1)
        mRs!H_inicial = "01:00:00"
        mRs!H_final = "01:00:00"
        mRs!add_date = Date
    End If
    
    mRs!C_CODIGO = pCodigo
    mRs!C_DESCRI = cPlato.Producto
    mRs!cu_Descripcion_Corta = Left(cPlato.Producto, mRs.Fields("cu_Descripcion_Corta").DefinedSize)
    mRs!c_Departamento = cPlato.Departamento
    mRs!c_Grupo = cPlato.Grupo
    mRs!c_Subgrupo = cPlato.SubGrupo
    mRs!n_CostoAct = cPlato.Costo
    mRs!n_Precio1 = cPlato.Precio
    mRs!n_Precio2 = cPlato.Precio
    mRs!n_Precio3 = cPlato.Precio
    mRs!n_Impuesto1 = cPlato.Impuesto
    mRs!n_Cantibul = 1
    
    'mMoneda = BuscarMonedaSistema(pCn)
    
    'If mMoneda <> vbNullString Then mRs!c_codmoneda = mMoneda
    
    mRs!c_CodMoneda = frmConfigPlatos.txtCategoria(3).Text
    
    If cPlato.Grupos(1).Items.Count = 0 Then 'n6 tiene items asociados no lo grabo como compuesto sino como unidad
        mRs!n_TipoPeso = 0 ' compuesto
    Else
        mRs!n_TipoPeso = 5 ' compuesto
    End If
    
    mRs!Update_Date = Date
    mRs.Update
    
    AgregarPendieteProd pCn, mRs
    
End Sub

Private Sub AgregarCodigo(pCn As ADODB.Connection, pCodigo As String, Optional TblPend As Boolean = False)
    
    Dim mSQl As String
    
    If Not TblPend Then
        
        mSQl = "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_CODIGOS " & _
        "(c_Codigo, c_Codnasa, c_Descripcion, n_Cantidad) " & _
        "VALUES " & _
        "('" & pCodigo & "', '" & pCodigo & "', 'Codigo Maestro', 1) "
        
    Else
        
        mSQl = "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PENDIENTE_CODIGO " & _
        "(c_Codigo, c_Codnasa, c_Descripcion, n_Cantidad, TipoCambio) " & _
        "VALUES " & _
        "('" & pCodigo & "', '" & pCodigo & "', 'Codigo Maestro', 1, 0) "
        
    End If
            
    pCn.Execute mSQl
    
End Sub

Private Sub AgregarPendieteProd(pCn As ADODB.Connection, pRs As ADODB.Recordset)
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    Dim mfield As ADODB.Field
    
    mSQl = "SELECT * FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PENDIENTE_PROD WHERE 1 = 2 "
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, pCn, adOpenDynamic, adLockOptimistic, adCmdText
    
    mRs.AddNew
        For Each mfield In pRs.Fields
            If StrComp(mfield.Name, "ID", vbTextCompare) <> 0 _
            And StrComp(mfield.Name, "c_FileImagen", vbTextCompare) <> 0 Then
                mRs.Fields(mfield.Name).Value = mfield.Value
            End If
        Next
        mRs!TipoCambio = 0
        mRs!Hablador = 1
    mRs.Update
    
End Sub

Private Sub GrabarPartes(pCn As ADODB.Connection, pCodigo As String)
    
    Dim mGrupo As clsGrupoPlato
    Dim mItm As clsItemGrupo
    
    Dim TrPend As Boolean
    
    TrPend = ExisteTablaV3("TR_PEND_PARTES", pCn, "" & FrmAppLink.Srv_Remote_BD_ADM & "")
    
    If mAplicaTRPend And TrPend Then
        
        Dim RsTrPend As ADODB.Recordset
        
        pCn.Execute "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PARTES " & _
        "(c_Codigo, c_Parte, n_Cantidad, TipoCambio) " & vbNewLine & _
        "SELECT c_Codigo, c_Parte, n_Cantidad, 1 AS TipoCambio " & _
        "FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PARTES WHERE c_Codigo = '" & pCodigo & "' ", TmpRecAffec
        
    End If
    
    pCn.Execute "DELETE FROM " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.MA_PARTES " & _
    "WHERE c_Codigo = '" & pCodigo & "' "
    
    For Each mGrupo In cPlato.Grupos
        
        If mGrupo.Sistema Then
            
            For Each mItm In mGrupo.Items
                
                mSQl = "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.MA_PARTES " & _
                "(c_Codigo, c_Parte, n_Cantidad) " & _
                "VALUES ('" & pCodigo & "', '" & mItm.CodigoProducto & "', " & mItm.Cantidad & ") "
                
                pCn.Execute mSQl
                
                If mAplicaTRPend And TrPend Then
                    
                    mSQl = "INSERT INTO " & FrmAppLink.Srv_Remote_BD_ADM & ".DBO.TR_PEND_PARTES (c_Codigo, c_Parte, n_Cantidad, TipoCambio) " & _
                    "VALUES (" & _
                    "'" & pCodigo & "', '" & mItm.CodigoProducto & "', " & mItm.Cantidad & ", 0) "
                    
                    pCn.Execute mSQl
                    
                End If
                
            Next
            
        End If
        
    Next
    
End Sub
