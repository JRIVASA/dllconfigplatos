VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsMensajeria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarMensaje                                                 As String
Private mvarBotonCancel                                             As Boolean
Private mvarRetorno                                                 As Boolean

Property Get MensajeMostrar() As String
    MensajeMostrar = mvarMensaje
End Property

Property Get BotonCancel() As Boolean
    BotonCancel = mvarBotonCancel
End Property

Property Get Retorno() As Boolean
    Retorno = mvarRetorno
End Property

Property Let Retorno(ByVal Data As Boolean)
    mvarRetorno = Data
End Property

Public Function Mensaje(ByVal Msg As String, Optional ByVal MostrarCancel As Boolean = False)
    
    On Error GoTo Errores
    
    mvarBotonCancel = MostrarCancel
    mvarMensaje = Msg
    
    Set frm_Mensajeria.fCls = Me
    
    frm_Mensajeria.Show vbModal
    
    Set frm_Mensajeria = Nothing
    
    Mensaje = mvarRetorno
    
Errores:
    
    Err.Clear
    
End Function
