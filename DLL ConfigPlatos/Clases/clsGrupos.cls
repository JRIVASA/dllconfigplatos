VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"clsGrupoPlato"
Attribute VB_Ext_KEY = "Member0" ,"clsGrupoPlato"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mCol                                                        As Collection

Public Function Add(CodigoPlato As Long, CodigoGrupo As Long, NombreGrupo As String, _
Requerido As Boolean, Costeable As Boolean, Cantidad As Double, _
Sistema As Boolean, Optional sKey As String) As clsGrupoPlato
    
    Dim objNewMember As clsGrupoPlato
    
    Set objNewMember = New clsGrupoPlato
    
    With objNewMember
        .Cantidad = Cantidad
        .CodigoGrupo = CodigoGrupo
        .CodigoPlato = CodigoPlato
        .Costeable = Costeable
        .NombreGrupo = NombreGrupo
        .Requerido = Requerido
        .Sistema = Sistema
    End With
    
    If Len(sKey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, sKey
    End If
    
    Set Add = objNewMember
    Set objNewMember = Nothing
    
End Function

Property Get Item(vntIndexKey As Variant) As clsGrupoPlato
Attribute Item.VB_UserMemId = 0
    Set Item = mCol(vntIndexKey)
End Property

Property Get Count() As Long
    Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub
