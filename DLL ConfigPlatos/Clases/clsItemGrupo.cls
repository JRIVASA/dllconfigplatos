VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsItemGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigoProducto                                              As String
Private mvarDescripcion                                                 As String
Private mvarCodigoGrupo                                                 As Long
Private mvarCodigoPlato                                                 As Long
Private mvarCantidad                                                    As Double
Private mvarCosto                                                       As Double
Private mvarPrecio                                                      As Double
Private mVarPrecioFicha                                                 As Double
Private mvarNumeroDec                                                   As Integer

Property Let NumeroDec(ByVal Valor As Integer)
    mvarNumeroDec = Valor
End Property

Property Get NumeroDec() As Integer
    NumeroDec = mvarNumeroDec
End Property

Property Let Cantidad(ByVal Valor As Double)
    mvarCantidad = Valor
End Property

Property Get Cantidad() As Double
    Cantidad = mvarCantidad
End Property

Property Let CodigoPlato(ByVal Valor As Long)
    mvarCodigoPlato = Valor
End Property

Property Get CodigoPlato() As Long
    CodigoPlato = mvarCodigoPlato
End Property

Property Let CodigoGrupo(ByVal Valor As Long)
    mvarCodigoGrupo = Valor
End Property

Property Get CodigoGrupo() As Long
    CodigoGrupo = mvarCodigoGrupo
End Property

Property Let CodigoProducto(ByVal Valor As String)
    mvarCodigoProducto = Valor
End Property

Property Get CodigoProducto() As String
    CodigoProducto = mvarCodigoProducto
End Property

Property Let Descripcion(ByVal Valor As String)
    mvarDescripcion = Valor
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Let Costo(ByVal Valor As Double)
    mvarCosto = Valor
End Property

Property Get Costo() As Double
    Costo = mvarCosto
End Property

Property Let Precio(ByVal Valor As Double)
    mvarPrecio = Valor
End Property

Property Get Precio() As Double
    Precio = mvarPrecio
End Property

Property Let PrecioFicha(ByVal Valor As Double)
    mVarPrecioFicha = Valor
End Property

Property Get PrecioFicha() As Double
    PrecioFicha = mVarPrecioFicha
End Property
