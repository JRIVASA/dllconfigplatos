VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPlato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarCodigoPlato                                             As Long
Private mvarUsaComentario                                           As Boolean
Private mvarCodigoProducto                                          As String
Private mvarProducto                                                As String
Private mvarCosto                                                   As Double
Private mvarPrecio                                                  As Double
Private mvarImpuesto                                                As Double
Private mvarDpto                                                    As String
Private mvarGrupo                                                   As String
Private mvarSubGrupo                                                As String
Private mGrupos                                                     As clsGrupos
Private mvarTiempoPreparacion                                       As Double
Private mvarInfoPreparacion                                         As String
Private mvarEnlacesAdicionales                                      As String

Property Get Grupos() As clsGrupos
    Set Grupos = mGrupos
End Property

Property Let Producto(ByVal Valor As String)
    mvarProducto = Valor
End Property

Property Get Producto() As String
    Producto = mvarProducto
End Property

Property Let CodigoProducto(ByVal Valor As String)
    mvarCodigoProducto = Valor
End Property

Property Get CodigoProducto() As String
    CodigoProducto = mvarCodigoProducto
End Property

Property Let CodigoPlato(ByVal Valor As Long)
    mvarCodigoPlato = Valor
End Property

Property Get CodigoPlato() As Long
    CodigoPlato = mvarCodigoPlato
End Property

Property Get UsaComentario() As Boolean
    UsaComentario = mvarUsaComentario
End Property

Property Let UsaComentario(ByVal Valor As Boolean)
    mvarUsaComentario = Valor
End Property

Property Get TiempoPreparacion() As Double
    TiempoPreparacion = mvarTiempoPreparacion
End Property

Property Let TiempoPreparacion(ByVal pValor As Double)
    mvarTiempoPreparacion = pValor
End Property

Property Get InfoPreparacion() As String
    InfoPreparacion = mvarInfoPreparacion
End Property

Property Let InfoPreparacion(ByVal pValor As String)
    mvarInfoPreparacion = pValor
End Property

Property Get EnlacesAdicionales() As String
    EnlacesAdicionales = mvarEnlacesAdicionales
End Property

Property Let EnlacesAdicionales(ByVal pValor As String)
    mvarEnlacesAdicionales = pValor
End Property

Property Get Costo() As Double
    Costo = mvarCosto
End Property

Property Get Precio() As Double
    Precio = mvarPrecio
End Property

Property Get Impuesto() As Double
    Impuesto = mvarImpuesto
End Property

Property Get Departamento() As String
    Departamento = mvarDpto
End Property

Property Get Grupo() As String
    Grupo = mvarGrupo
End Property

Property Get SubGrupo() As String
    SubGrupo = mvarSubGrupo
End Property

Property Let Costo(pValor As Double)
    mvarCosto = pValor
End Property

Property Let Precio(pValor As Double)
    mvarPrecio = pValor
End Property

Property Let Impuesto(pValor As Double)
    mvarImpuesto = pValor
End Property

Property Let Departamento(pValor As String)
    mvarDpto = pValor
End Property

Property Let Grupo(pValor As String)
    mvarGrupo = pValor
End Property

Property Let SubGrupo(pValor As String)
    mvarSubGrupo = pValor
End Property

Private Sub Class_Initialize()
    Set mGrupos = New clsGrupos
End Sub

Private Sub Class_Terminate()
    Set mGrupos = Nothing
End Sub
